<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Exception and Error Handling in Oct-Files (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Exception and Error Handling in Oct-Files (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Exception and Error Handling in Oct-Files (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Oct_002dFiles.html" rel="up" title="Oct-Files">
<link href="Documentation-and-Testing-of-Oct_002dFiles.html" rel="next" title="Documentation and Testing of Oct-Files">
<link href="Input-Parameter-Checking-in-Oct_002dFiles.html" rel="prev" title="Input Parameter Checking in Oct-Files">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
kbd.key {font-style: normal}
span:hover a.copiable-link {visibility: visible}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="subsection-level-extent" id="Exception-and-Error-Handling-in-Oct_002dFiles">
<div class="nav-panel">
<p>
Next: <a href="Documentation-and-Testing-of-Oct_002dFiles.html" accesskey="n" rel="next">Documentation and Testing of Oct-Files</a>, Previous: <a href="Input-Parameter-Checking-in-Oct_002dFiles.html" accesskey="p" rel="prev">Input Parameter Checking in Oct-Files</a>, Up: <a href="Oct_002dFiles.html" accesskey="u" rel="up">Oct-Files</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Exception-and-Error-Handling-in-Oct_002dFiles-1"><span>A.1.12 Exception and Error Handling in Oct-Files<a class="copiable-link" href="#Exception-and-Error-Handling-in-Oct_002dFiles-1"> &para;</a></span></h4>

<p>Another important feature of Octave is its ability to react to the user typing
<kbd class="key">Control-C</kbd> during extended calculations.  This ability is based on the C++
exception handler, where memory allocated by the C++ new/delete methods is
automatically released when the exception is treated.  When writing an oct-file
which may run for a long time the programmer must periodically use the macro
<code class="code">OCTAVE_QUIT</code><!-- /@w -->, in order to allow Octave to check and possibly respond
to a user typing <kbd class="key">Control-C</kbd>.  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">for (octave_idx_type i = 0; i &lt; a.nelem (); i++)
  {
    OCTAVE_QUIT;
    b.elem (i) = 2. * a.elem (i);
  }
</pre></div></div>

<p>The presence of the <code class="code">OCTAVE_QUIT</code><!-- /@w -->&nbsp;macro in the inner loop allows
Octave to detect and acknowledge a <kbd class="key">Control-C</kbd> key sequence.  Without this
macro, the user must either wait for the oct-file function to return before the
interrupt is processed, or the user must press <kbd class="key">Control-C</kbd> three times
which will force Octave to exit completely.
</p>
<p>The <code class="code">OCTAVE_QUIT</code><!-- /@w -->&nbsp;macro does impose a very small performance penalty;
For loops that are known to be small it may not make sense to include
<code class="code">OCTAVE_QUIT</code><!-- /@w -->.
</p>
<p>When creating an oct-file that uses an external library, the function might
spend a significant portion of its time in the external library.  It is not
generally possible to use the <code class="code">OCTAVE_QUIT</code><!-- /@w -->&nbsp;macro in this case.  The
alternative code in this case is
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">BEGIN_INTERRUPT_IMMEDIATELY_IN_FOREIGN_CODE;
...  some code that calls a &quot;foreign&quot; function ...
END_INTERRUPT_IMMEDIATELY_IN_FOREIGN_CODE;
</pre></div></div>

<p>The disadvantage of this is that if the foreign code allocates any memory
internally, then this memory might be lost during an interrupt, without being
deallocated.  Therefore, ideally Octave itself should allocate any memory that
is needed by the foreign code, with either the <code class="code">fortran_vec</code>
method or the <code class="code">OCTAVE_LOCAL_BUFFER</code><!-- /@w -->&nbsp;macro.
</p>
<p>The Octave <code class="code">unwind_protect</code> mechanism (<a class="ref" href="The-unwind_005fprotect-Statement.html">The unwind_protect Statement</a>)
can also be used in oct-files.  In conjunction with the exception handling of
Octave, it ensures that certain recovery code is always run even if an
exception occurs.  An example of the use of this mechanism is
</p>
<div class="example">
<pre class="verbatim">#include &lt;octave/oct.h&gt;
#include &lt;octave/unwind-prot.h&gt;

void
my_err_handler (const char *fmt, ...)
{
  // Do nothing!!
}

void
my_err_with_id_handler (const char *id, const char *fmt, ...)
{
  // Do nothing!!
}

DEFUN_DLD (unwinddemo, args, nargout, &quot;Unwind Demo&quot;)
{
  if (args.length () &lt; 2)
    print_usage ();

  NDArray a = args(0).array_value ();
  NDArray b = args(1).array_value ();

  // Create unwind_action objects.  At the end of the enclosing scope,
  // destructors for these objects will call the given functions with
  // the specified arguments.

  octave::unwind_action restore_warning_handler
    (set_liboctave_warning_handler, current_liboctave_warning_handler);

  octave::unwind_action restore_warning_with_id_handler
    (set_liboctave_warning_with_id_handler,
     current_liboctave_warning_with_id_handler);

  set_liboctave_warning_handler (my_err_handler);
  set_liboctave_warning_with_id_handler (my_err_with_id_handler);

  return octave_value (quotient (a, b));
}
</pre></div>

<p>As can be seen in the example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">unwinddemo (1, 0)
&rArr; Inf
1 / 0
&rArr; warning: division by zero
   Inf
</pre></div></div>

<p>The warning for division by zero (and in fact all warnings) are disabled in the
<code class="code">unwinddemo</code> function.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Documentation-and-Testing-of-Oct_002dFiles.html">Documentation and Testing of Oct-Files</a>, Previous: <a href="Input-Parameter-Checking-in-Oct_002dFiles.html">Input Parameter Checking in Oct-Files</a>, Up: <a href="Oct_002dFiles.html">Oct-Files</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
