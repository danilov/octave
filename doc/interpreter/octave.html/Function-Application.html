<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Function Application (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Function Application (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Function Application (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Vectorization-and-Faster-Code-Execution.html" rel="up" title="Vectorization and Faster Code Execution">
<link href="Accumulation.html" rel="next" title="Accumulation">
<link href="Broadcasting.html" rel="prev" title="Broadcasting">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="Function-Application">
<div class="nav-panel">
<p>
Next: <a href="Accumulation.html" accesskey="n" rel="next">Accumulation</a>, Previous: <a href="Broadcasting.html" accesskey="p" rel="prev">Broadcasting</a>, Up: <a href="Vectorization-and-Faster-Code-Execution.html" accesskey="u" rel="up">Vectorization and Faster Code Execution</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Function-Application-1"><span>19.3 Function Application<a class="copiable-link" href="#Function-Application-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-map"></a>
<a class="index-entry-id" id="index-apply"></a>
<a class="index-entry-id" id="index-function-application"></a>

<p>As a general rule, functions should already be written with matrix
arguments in mind and should consider whole matrix operations in a
vectorized manner.  Sometimes, writing functions in this way appears
difficult or impossible for various reasons.  For those situations,
Octave provides facilities for applying a function to each element of an
array, cell, or struct.
</p>
<a class="anchor" id="XREFarrayfun"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-arrayfun"><span class="category-def">: </span><span><code class="def-type"><var class="var">B</var> =</code> <strong class="def-name">arrayfun</strong> <code class="def-code-arguments">(<var class="var">fcn</var>, <var class="var">A</var>)</code><a class="copiable-link" href="#index-arrayfun"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-arrayfun-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">B</var> =</code> <strong class="def-name">arrayfun</strong> <code class="def-code-arguments">(<var class="var">fcn</var>, <var class="var">A1</var>, <var class="var">A2</var>, &hellip;)</code><a class="copiable-link" href="#index-arrayfun-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-arrayfun-2"><span class="category-def">: </span><span><code class="def-type">[<var class="var">B1</var>, <var class="var">B2</var>, &hellip;] =</code> <strong class="def-name">arrayfun</strong> <code class="def-code-arguments">(<var class="var">fcn</var>, <var class="var">A</var>, &hellip;)</code><a class="copiable-link" href="#index-arrayfun-2"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-arrayfun-3"><span class="category-def">: </span><span><code class="def-type"><var class="var">B</var> =</code> <strong class="def-name">arrayfun</strong> <code class="def-code-arguments">(&hellip;, &quot;UniformOutput&quot;, <var class="var">val</var>)</code><a class="copiable-link" href="#index-arrayfun-3"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-arrayfun-4"><span class="category-def">: </span><span><code class="def-type"><var class="var">B</var> =</code> <strong class="def-name">arrayfun</strong> <code class="def-code-arguments">(&hellip;, &quot;ErrorHandler&quot;, <var class="var">errfcn</var>)</code><a class="copiable-link" href="#index-arrayfun-4"> &para;</a></span></dt>
<dd>
<p>Execute a function on each element of an array.
</p>
<p>This is useful for functions that do not accept array arguments.  If the
function does accept array arguments it is <em class="emph">better</em> to call the function
directly.
</p>
<p>The first input argument <var class="var">fcn</var> can be a string, a function handle, an
inline function, or an anonymous function.  The input argument <var class="var">A</var> can be a
logical array, a numeric array, a string array, a structure array, or a cell
array.  <code class="code">arrayfun</code> passes all elements of <var class="var">A</var> individually to the
function <var class="var">fcn</var> and collects the results.  The equivalent pseudo-code is
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">cls = class (<var class="var">fcn</var> (<var class="var">A</var>(1));
<var class="var">B</var> = zeros (size (<var class="var">A</var>), cls);
for i = 1:numel (<var class="var">A</var>)
  <var class="var">B</var>(i) = <var class="var">fcn</var> (<var class="var">A</var>(i))
endfor
</pre></div></div>

<p>The named function can also take more than two input arguments, with the input
arguments given as third input argument <var class="var">A2</var>, fourth input argument
<var class="var">A2</var>, &hellip;  If given more than one array input argument then all input
arguments must have the same sizes.  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">arrayfun (@atan2, [1, 0], [0, 1])
     &rArr; [ 1.57080   0.00000 ]
</pre></div></div>

<p>If the parameter <var class="var">val</var> after a further string input argument
<code class="code">&quot;UniformOutput&quot;</code> is set <code class="code">true</code> (the default), then the named
function <var class="var">fcn</var> must return a single element which then will be concatenated
into the return value and is of type matrix.  Otherwise, if that parameter is
set to <code class="code">false</code>, then the outputs are concatenated in a cell array.  For
example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">arrayfun (@(x,y) x:y, &quot;abc&quot;, &quot;def&quot;, &quot;UniformOutput&quot;, false)
&rArr;
   {
     [1,1] = abcd
     [1,2] = bcde
     [1,3] = cdef
   }
</pre></div></div>

<p>If more than one output arguments are given then the named function must return
the number of return values that also are expected, for example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">[A, B, C] = arrayfun (@find, [10; 0], &quot;UniformOutput&quot;, false)
&rArr;
A =
{
   [1,1] =  1
   [2,1] = [](0x0)
}
B =
{
   [1,1] =  1
   [2,1] = [](0x0)
}
C =
{
   [1,1] =  10
   [2,1] = [](0x0)
}
</pre></div></div>

<p>If the parameter <var class="var">errfcn</var> after a further string input argument
<code class="code">&quot;ErrorHandler&quot;</code> is another string, a function handle, an inline
function, or an anonymous function, then <var class="var">errfcn</var> defines a function to
call in the case that <var class="var">fcn</var> generates an error.  The definition of the
function must be of the form
</p>
<div class="example">
<pre class="example-preformatted">function [...] = errfcn (<var class="var">s</var>, ...)
</pre></div>

<p>where there is an additional input argument to <var class="var">errfcn</var> relative to
<var class="var">fcn</var>, given by <var class="var">s</var>.  This is a structure with the elements
<code class="code">&quot;identifier&quot;</code>, <code class="code">&quot;message&quot;</code>, and <code class="code">&quot;index&quot;</code> giving,
respectively, the error identifier, the error message, and the index of the
array elements that caused the error.  The size of the output argument of
<var class="var">errfcn</var> must have the same size as the output argument of <var class="var">fcn</var>,
otherwise a real error is thrown.  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">function y = ferr (s, x), y = &quot;MyString&quot;; endfunction
arrayfun (@str2num, [1234],
          &quot;UniformOutput&quot;, false, &quot;ErrorHandler&quot;, @ferr)
&rArr;
   {
     [1,1] = MyString
   }
</pre></div></div>


<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFspfun">spfun</a>, <a class="ref" href="#XREFcellfun">cellfun</a>, <a class="ref" href="#XREFstructfun">structfun</a>.
</p></dd></dl>


<a class="anchor" id="XREFspfun"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-spfun"><span class="category-def">: </span><span><code class="def-type"><var class="var">y</var> =</code> <strong class="def-name">spfun</strong> <code class="def-code-arguments">(<var class="var">f</var>, <var class="var">S</var>)</code><a class="copiable-link" href="#index-spfun"> &para;</a></span></dt>
<dd><p>Compute <code class="code">f (<var class="var">S</var>)</code> for the nonzero elements of <var class="var">S</var>.
</p>
<p>The input function <var class="var">f</var> is applied only to the nonzero elements of
the input matrix <var class="var">S</var> which is typically sparse.  The function <var class="var">f</var>
can be passed as a string, function handle, or inline function.
</p>
<p>The output <var class="var">y</var> is a sparse matrix with the same sparsity structure as
the input <var class="var">S</var>.  <code class="code">spfun</code> preserves sparsity structure which is
different than simply applying the function <var class="var">f</var> to the sparse matrix
<var class="var">S</var> when <code class="code"><var class="var">f</var> (0) != 0</code>.
</p>
<p>Example
</p>
<p>Sparsity preserving <code class="code">spfun</code> versus normal function application
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">S = pi * speye (2,2)
S =

Compressed Column Sparse (rows = 2, cols = 2, nnz = 2 [50%])

  (1, 1) -&gt; 3.1416
  (2, 2) -&gt; 3.1416

y = spfun (@cos, S)
y =

Compressed Column Sparse (rows = 2, cols = 2, nnz = 2 [50%])

  (1, 1) -&gt; -1
  (2, 2) -&gt; -1
</pre></div><pre class="example-preformatted">

</pre><div class="group"><pre class="example-preformatted">y = cos (S)
y =

Compressed Column Sparse (rows = 2, cols = 2, nnz = 4 [100%])

  (1, 1) -&gt; -1
  (2, 1) -&gt; 1
  (1, 2) -&gt; 1
  (2, 2) -&gt; -1

</pre></div></div>

<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFarrayfun">arrayfun</a>, <a class="ref" href="#XREFcellfun">cellfun</a>, <a class="ref" href="#XREFstructfun">structfun</a>.
</p></dd></dl>


<a class="anchor" id="XREFcellfun"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-cellfun"><span class="category-def">: </span><span><code class="def-type"><var class="var">A</var> =</code> <strong class="def-name">cellfun</strong> <code class="def-code-arguments">(&quot;<var class="var">fcn</var>&quot;, <var class="var">C</var>)</code><a class="copiable-link" href="#index-cellfun"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-cellfun-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">A</var> =</code> <strong class="def-name">cellfun</strong> <code class="def-code-arguments">(&quot;size&quot;, <var class="var">C</var>, <var class="var">k</var>)</code><a class="copiable-link" href="#index-cellfun-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-cellfun-2"><span class="category-def">: </span><span><code class="def-type"><var class="var">A</var> =</code> <strong class="def-name">cellfun</strong> <code class="def-code-arguments">(&quot;isclass&quot;, <var class="var">C</var>, <var class="var">class</var>)</code><a class="copiable-link" href="#index-cellfun-2"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-cellfun-3"><span class="category-def">: </span><span><code class="def-type"><var class="var">A</var> =</code> <strong class="def-name">cellfun</strong> <code class="def-code-arguments">(@<var class="var">fcn</var>, <var class="var">C</var>)</code><a class="copiable-link" href="#index-cellfun-3"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-cellfun-4"><span class="category-def">: </span><span><code class="def-type"><var class="var">A</var> =</code> <strong class="def-name">cellfun</strong> <code class="def-code-arguments">(<var class="var">fcn</var>, <var class="var">C</var>)</code><a class="copiable-link" href="#index-cellfun-4"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-cellfun-5"><span class="category-def">: </span><span><code class="def-type"><var class="var">A</var> =</code> <strong class="def-name">cellfun</strong> <code class="def-code-arguments">(<var class="var">fcn</var>, <var class="var">C1</var>, <var class="var">C2</var>, &hellip;)</code><a class="copiable-link" href="#index-cellfun-5"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-cellfun-6"><span class="category-def">: </span><span><code class="def-type">[<var class="var">A1</var>, <var class="var">A2</var>, &hellip;] =</code> <strong class="def-name">cellfun</strong> <code class="def-code-arguments">(&hellip;)</code><a class="copiable-link" href="#index-cellfun-6"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-cellfun-7"><span class="category-def">: </span><span><code class="def-type"><var class="var">A</var> =</code> <strong class="def-name">cellfun</strong> <code class="def-code-arguments">(&hellip;, &quot;ErrorHandler&quot;, <var class="var">errfcn</var>)</code><a class="copiable-link" href="#index-cellfun-7"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-cellfun-8"><span class="category-def">: </span><span><code class="def-type"><var class="var">A</var> =</code> <strong class="def-name">cellfun</strong> <code class="def-code-arguments">(&hellip;, &quot;UniformOutput&quot;, <var class="var">val</var>)</code><a class="copiable-link" href="#index-cellfun-8"> &para;</a></span></dt>
<dd>
<p>Evaluate the function named &quot;<var class="var">fcn</var>&quot; on the elements of the cell array
<var class="var">C</var>.
</p>
<p>Elements in <var class="var">C</var> are passed on to the named function individually.  The
function <var class="var">fcn</var> can be one of the functions
</p>
<dl class="table">
<dt><code class="code">isempty</code></dt>
<dd><p>Return 1 for empty elements.
</p>
</dd>
<dt><code class="code">islogical</code></dt>
<dd><p>Return 1 for logical elements.
</p>
</dd>
<dt><code class="code">isnumeric</code></dt>
<dd><p>Return 1 for numeric elements.
</p>
</dd>
<dt><code class="code">isreal</code></dt>
<dd><p>Return 1 for real elements.
</p>
</dd>
<dt><code class="code">length</code></dt>
<dd><p>Return a vector of the lengths of cell elements.
</p>
</dd>
<dt><code class="code">ndims</code></dt>
<dd><p>Return the number of dimensions of each element.
</p>
</dd>
<dt><code class="code">numel</code></dt>
<dt><code class="code">prodofsize</code></dt>
<dd><p>Return the number of elements contained within each cell element.  The
number is the product of the dimensions of the object at each cell element.
</p>
</dd>
<dt><code class="code">size</code></dt>
<dd><p>Return the size along the <var class="var">k</var>-th dimension.
</p>
</dd>
<dt><code class="code">isclass</code></dt>
<dd><p>Return 1 for elements of <var class="var">class</var>.
</p></dd>
</dl>

<p>Additionally, <code class="code">cellfun</code> accepts an arbitrary function <var class="var">fcn</var> in the
form of an inline function, function handle, or the name of a function (in a
character string).  The function can take one or more arguments, with the
inputs arguments given by <var class="var">C1</var>, <var class="var">C2</var>, etc.  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">cellfun (&quot;atan2&quot;, {1, 0}, {0, 1})
     &rArr; [ 1.57080   0.00000 ]
</pre></div></div>

<p>The number of output arguments of <code class="code">cellfun</code> matches the number of output
arguments of the function and can be greater than one.  When there are multiple
outputs of the function they will be collected into the output arguments of
<code class="code">cellfun</code> like this:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">function [a, b] = twoouts (x)
  a = x;
  b = x*x;
endfunction
[aa, bb] = cellfun (@twoouts, {1, 2, 3})
     &rArr;
        aa =
           1 2 3
        bb =
           1 4 9
</pre></div></div>

<p>Note that, per default, the output argument(s) are arrays of the same size as
the input arguments.  Input arguments that are singleton (1x1) cells will be
automatically expanded to the size of the other arguments.
</p>
<p>If the parameter <code class="code">&quot;UniformOutput&quot;</code> is set to true (the default), then the
function must return scalars which will be concatenated into the return
array(s).  If <code class="code">&quot;UniformOutput&quot;</code> is false, the outputs are concatenated
into a cell array (or cell arrays).  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">cellfun (&quot;lower&quot;, {&quot;Foo&quot;, &quot;Bar&quot;, &quot;FooBar&quot;},
         &quot;UniformOutput&quot;, false)
&rArr; {&quot;foo&quot;, &quot;bar&quot;, &quot;foobar&quot;}
</pre></div></div>

<p>Given the parameter <code class="code">&quot;ErrorHandler&quot;</code>, then <var class="var">errfcn</var> defines a
function to call in case <var class="var">fcn</var> generates an error.  The form of the
function is
</p>
<div class="example">
<pre class="example-preformatted">function [...] = errfcn (<var class="var">s</var>, ...)
</pre></div>

<p>where there is an additional input argument to <var class="var">errfcn</var> relative to
<var class="var">fcn</var>, given by <var class="var">s</var>.  This is a structure with the elements
<code class="code">&quot;identifier&quot;</code>, <code class="code">&quot;message&quot;</code>, and <code class="code">&quot;index&quot;</code> giving
respectively the error identifier, the error message, and the index into the
input arguments of the element that caused the error.  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">function y = foo (s, x), y = NaN; endfunction
cellfun (&quot;factorial&quot;, {-1,2}, &quot;ErrorHandler&quot;, @foo)
&rArr; [NaN 2]
</pre></div></div>

<p>Use <code class="code">cellfun</code> intelligently.  The <code class="code">cellfun</code> function is a useful tool
for avoiding loops.  It is often used with anonymous function handles; however,
calling an anonymous function involves an overhead quite comparable to the
overhead of an m-file function.  Passing a handle to a built-in function is
faster, because the interpreter is not involved in the internal loop.  For
example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">C = {...}
v = cellfun (@(x) det (x), C); # compute determinants
v = cellfun (@det, C);         # 40% faster
</pre></div></div>


<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFarrayfun">arrayfun</a>, <a class="ref" href="#XREFstructfun">structfun</a>, <a class="ref" href="#XREFspfun">spfun</a>.
</p></dd></dl>


<a class="anchor" id="XREFstructfun"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-structfun"><span class="category-def">: </span><span><code class="def-type"><var class="var">A</var> =</code> <strong class="def-name">structfun</strong> <code class="def-code-arguments">(<var class="var">fcn</var>, <var class="var">S</var>)</code><a class="copiable-link" href="#index-structfun"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-structfun-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">A</var> =</code> <strong class="def-name">structfun</strong> <code class="def-code-arguments">(&hellip;, &quot;ErrorHandler&quot;, <var class="var">errfcn</var>)</code><a class="copiable-link" href="#index-structfun-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-structfun-2"><span class="category-def">: </span><span><code class="def-type"><var class="var">A</var> =</code> <strong class="def-name">structfun</strong> <code class="def-code-arguments">(&hellip;, &quot;UniformOutput&quot;, <var class="var">val</var>)</code><a class="copiable-link" href="#index-structfun-2"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-structfun-3"><span class="category-def">: </span><span><code class="def-type">[<var class="var">A</var>, <var class="var">B</var>, &hellip;] =</code> <strong class="def-name">structfun</strong> <code class="def-code-arguments">(&hellip;)</code><a class="copiable-link" href="#index-structfun-3"> &para;</a></span></dt>
<dd>
<p>Evaluate the function named <var class="var">name</var> on the fields of the structure
<var class="var">S</var>.  The fields of <var class="var">S</var> are passed to the function <var class="var">fcn</var>
individually.
</p>
<p><code class="code">structfun</code> accepts an arbitrary function <var class="var">fcn</var> in the form of an
inline function, function handle, or the name of a function (in a character
string).  In the case of a character string argument, the function must
accept a single argument named <var class="var">x</var>, and it must return a string value.
If the function returns more than one argument, they are returned as
separate output variables.
</p>
<p>If the parameter <code class="code">&quot;UniformOutput&quot;</code> is set to true (the default), then
the function must return a single element which will be concatenated into
the return value.  If <code class="code">&quot;UniformOutput&quot;</code> is false, the outputs are
placed into a structure with the same fieldnames as the input structure.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">s.name1 = &quot;John Smith&quot;;
s.name2 = &quot;Jill Jones&quot;;
structfun (@(x) regexp (x, '(\w+)$', &quot;matches&quot;){1}, s,
           &quot;UniformOutput&quot;, false)
  &rArr; scalar structure containing the fields:
       name1 = Smith
       name2 = Jones
</pre></div></div>

<p>Given the parameter <code class="code">&quot;ErrorHandler&quot;</code>, <var class="var">errfcn</var> defines a function
to call in case <var class="var">fcn</var> generates an error.  The form of the function is
</p>
<div class="example">
<pre class="example-preformatted">function [...] = errfcn (<var class="var">se</var>, ...)
</pre></div>

<p>where there is an additional input argument to <var class="var">errfcn</var> relative to
<var class="var">fcn</var>, given by <var class="var">se</var>.  This is a structure with the
elements <code class="code">&quot;identifier&quot;</code>, <code class="code">&quot;message&quot;</code> and <code class="code">&quot;index&quot;</code>,
giving respectively the error identifier, the error message, and the index
into the input arguments of the element that caused the error.  For an
example on how to use an error handler, see <a class="pxref" href="#XREFcellfun"><code class="code">cellfun</code></a>.
</p>

<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFcellfun">cellfun</a>, <a class="ref" href="#XREFarrayfun">arrayfun</a>, <a class="ref" href="#XREFspfun">spfun</a>.
</p></dd></dl>


<p>Consistent with earlier advice, seek to use Octave built-in functions whenever
possible for the best performance.  This advice applies especially to the four
functions above.  For example, when adding two arrays together
element-by-element one could use a handle to the built-in addition function
<code class="code">@plus</code> or define an anonymous function <code class="code">@(x,y) x + y</code>.  But, the
anonymous function is 60% slower than the first method.
See <a class="xref" href="Operator-Overloading.html">Operator Overloading</a>, for a list of basic functions which might be used
in place of anonymous ones.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Accumulation.html">Accumulation</a>, Previous: <a href="Broadcasting.html">Broadcasting</a>, Up: <a href="Vectorization-and-Faster-Code-Execution.html">Vectorization and Faster Code Execution</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
