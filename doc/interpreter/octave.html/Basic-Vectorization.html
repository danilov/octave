<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Basic Vectorization (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Basic Vectorization (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Basic Vectorization (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Vectorization-and-Faster-Code-Execution.html" rel="up" title="Vectorization and Faster Code Execution">
<link href="Broadcasting.html" rel="next" title="Broadcasting">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="Basic-Vectorization">
<div class="nav-panel">
<p>
Next: <a href="Broadcasting.html" accesskey="n" rel="next">Broadcasting</a>, Up: <a href="Vectorization-and-Faster-Code-Execution.html" accesskey="u" rel="up">Vectorization and Faster Code Execution</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Basic-Vectorization-1"><span>19.1 Basic Vectorization<a class="copiable-link" href="#Basic-Vectorization-1"> &para;</a></span></h3>

<p>To a very good first approximation, the goal in vectorization is to
write code that avoids loops and uses whole-array operations.  As a
trivial example, consider
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">for i = 1:n
  for j = 1:m
    c(i,j) = a(i,j) + b(i,j);
  endfor
endfor
</pre></div></div>

<p>compared to the much simpler
</p>
<div class="example">
<pre class="example-preformatted">c = a + b;
</pre></div>

<p>This isn&rsquo;t merely easier to write; it is also internally much easier to
optimize.  Octave delegates this operation to an underlying
implementation which, among other optimizations, may use special vector
hardware instructions or could conceivably even perform the additions in
parallel.  In general, if the code is vectorized, the underlying
implementation has more freedom about the assumptions it can make
in order to achieve faster execution.
</p>
<p>This is especially important for loops with &quot;cheap&quot; bodies.  Often it
suffices to vectorize just the innermost loop to get acceptable
performance.  A general rule of thumb is that the &quot;order&quot; of the
vectorized body should be greater or equal to the &quot;order&quot; of the
enclosing loop.
</p>
<p>As a less trivial example, instead of
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">for i = 1:n-1
  a(i) = b(i+1) - b(i);
endfor
</pre></div></div>

<p>write
</p>
<div class="example">
<pre class="example-preformatted">a = b(2:n) - b(1:n-1);
</pre></div>

<p>This shows an important general concept about using arrays for indexing
instead of looping over an index variable.  See <a class="xref" href="Index-Expressions.html">Index Expressions</a>.
Also use boolean indexing generously.  If a condition needs to be tested,
this condition can also be written as a boolean index.  For instance,
instead of
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">for i = 1:n
  if (a(i) &gt; 5)
    a(i) -= 20
  endif
endfor
</pre></div></div>

<p>write
</p>
<div class="example">
<pre class="example-preformatted">a(a&gt;5) -= 20;
</pre></div>

<p>which exploits the fact that <code class="code">a &gt; 5</code> produces a boolean index.
</p>
<p>Use elementwise vector operators whenever possible to avoid looping
(operators like <code class="code">.*</code> and <code class="code">.^</code>).  See <a class="xref" href="Arithmetic-Ops.html">Arithmetic Operators</a>.
</p>
<p>Also exploit broadcasting in these elementwise operators both to avoid
looping and unnecessary intermediate memory allocations.
See <a class="xref" href="Broadcasting.html">Broadcasting</a>.
</p>
<p>Use built-in and library functions if possible.  Built-in and compiled
functions are very fast.  Even with an m-file library function, chances
are good that it is already optimized, or will be optimized more in a
future release.
</p>
<p>For instance, even better than
</p>
<div class="example">
<pre class="example-preformatted">a = b(2:n) - b(1:n-1);
</pre></div>

<p>is
</p>
<div class="example">
<pre class="example-preformatted">a = diff (b);
</pre></div>

<p>Most Octave functions are written with vector and array arguments in
mind.  If you find yourself writing a loop with a very simple operation,
chances are that such a function already exists.  The following functions
occur frequently in vectorized code:
</p>
<ul class="itemize mark-bullet">
<li>Index manipulation

<ul class="itemize mark-bullet">
<li>find

</li><li>sub2ind

</li><li>ind2sub

</li><li>sort

</li><li>unique

</li><li>lookup

</li><li>ifelse / merge
</li></ul>

</li><li>Repetition

<ul class="itemize mark-bullet">
<li>repmat

</li><li>repelems
</li></ul>

</li><li>Vectorized arithmetic

<ul class="itemize mark-bullet">
<li>sum

</li><li>prod

</li><li>cumsum

</li><li>cumprod

</li><li>sumsq

</li><li>diff

</li><li>dot

</li><li>cummax

</li><li>cummin
</li></ul>

</li><li>Shape of higher dimensional arrays

<ul class="itemize mark-bullet">
<li>reshape

</li><li>resize

</li><li>permute

</li><li>squeeze

</li><li>deal
</li></ul>

</li></ul>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Broadcasting.html">Broadcasting</a>, Up: <a href="Vectorization-and-Faster-Code-Execution.html">Vectorization and Faster Code Execution</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
