<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Installing and Removing Packages (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Installing and Removing Packages (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Installing and Removing Packages (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Packages.html" rel="up" title="Packages">
<link href="Using-Packages.html" rel="next" title="Using Packages">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="Installing-and-Removing-Packages">
<div class="nav-panel">
<p>
Next: <a href="Using-Packages.html" accesskey="n" rel="next">Using Packages</a>, Up: <a href="Packages.html" accesskey="u" rel="up">Packages</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Installing-and-Removing-Packages-1"><span>37.1 Installing and Removing Packages<a class="copiable-link" href="#Installing-and-Removing-Packages-1"> &para;</a></span></h3>

<p>Assuming a package is available in the file <samp class="file">image-1.0.0.tar.gz</samp>
it can be installed from the Octave prompt with the command
</p>
<div class="example">
<pre class="example-preformatted">pkg install image-1.0.0.tar.gz
</pre></div>

<p>If the package is installed successfully nothing will be printed on
the prompt, but if a warning or error occurred during installation it
will be reported.  It is possible to install several packages at once
by writing several package file names after the <code class="code">pkg install</code>
command.  If a different version of the package is already installed it
will be removed prior to installing the new package.  This makes it
easy to upgrade and downgrade the version of a package, but makes it
impossible to have several versions of the same package installed at
once.
</p>
<p>To see which packages are installed type
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">pkg list
-| Package Name  | Version | Installation directory
-| --------------+---------+-----------------------
-|        image *|   1.0.0 | /home/jwe/octave/image-1.0.0
</pre></div></div>

<p>In this case, version 1.0.0 of the <code class="code">image</code> package is installed.
The <code class="code">'*'</code> character next to the package name shows that the image
package is loaded and ready for use.
</p>
<p>It is possible to remove a package from the system using the
<code class="code">pkg uninstall</code> command like this
</p>
<div class="example">
<pre class="example-preformatted">pkg uninstall image
</pre></div>

<p>If the package is removed successfully nothing will be printed in the
prompt, but if a warning or error occurred it will be reported.  It
should be noted that the package file used for installation is not
needed for removal, and that only the package name as reported by
<code class="code">pkg list</code> should be used when removing a package.  It is possible
to remove several packages at once by writing several package names
after the <code class="code">pkg uninstall</code> command.
</p>
<p>To minimize the amount of code duplication between packages, it is
possible that one package depends on another one.  If a package
depends on another, it will check if that package is installed
during installation.  If it is not, an error will be reported and
the package will not be installed.  This behavior can be disabled
by passing the <samp class="option">-nodeps</samp> flag to the <code class="code">pkg install</code>
command
</p>
<div class="example">
<pre class="example-preformatted">pkg install -nodeps my_package_with_dependencies.tar.gz
</pre></div>

<p>Since the installed package expects its dependencies to be installed
it may not function correctly.  Because of this it is not recommended
to disable dependency checking.
</p>
<a class="anchor" id="XREFpkg"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-pkg-1"><span class="category-def">: </span><span><strong class="def-name">pkg</strong> <code class="def-code-arguments"><var class="var">command</var> <var class="var">pkg_name</var></code><a class="copiable-link" href="#index-pkg-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-pkg-2"><span class="category-def">: </span><span><strong class="def-name">pkg</strong> <code class="def-code-arguments"><var class="var">command</var> <var class="var">option</var> <var class="var">pkg_name</var></code><a class="copiable-link" href="#index-pkg-2"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-pkg-3"><span class="category-def">: </span><span><code class="def-type">[<var class="var">out1</var>, &hellip;] =</code> <strong class="def-name">pkg</strong> <code class="def-code-arguments">(<var class="var">command</var>, &hellip; )</code><a class="copiable-link" href="#index-pkg-3"> &para;</a></span></dt>
<dd><p>Manage or query packages (groups of add-on functions) for Octave.
</p>
<p>Packages can be installed globally (i.e., for all users of the system) or
locally (i.e., for the current user only).
</p>
<p>Global packages are installed by default in a system-wide location.  This is
usually a subdirectory of the folder where Octave itself is installed.
Therefore, Octave needs write access to this folder to install global
packages, which is usually only available when Octave is run with
administrative privileges, such as when run as root (or superuser) on
Unix-like systems, or run with elevated privileges (&quot;Run as administrator&quot;)
on Windows.
</p>
<p>In contrast, local packages are installed by default in the user&rsquo;s home
directory (or user profile on Windows) and are only available to that
specific user.  Usually, they can be installed without administrative
privileges.
</p>
<p>When Octave is running with administrative privileges, <code class="code">pkg</code> will
install packages to the global package location by default.  Otherwise,
packages will be installed to the local location by default.  The user can
override this default installation location with optional arguments
(<samp class="option">-local</samp> or <samp class="option">-global</samp>) as described below.  The currently
used default package installation location can be queried with
<code class="code">pkg prefix</code>.
</p>
<p>For global and local packages, there are separate databases holding the
information about the installed packages.  If some package is installed
globally as well as locally, the local installation takes precedence over
(&quot;shadows&quot;) the global one.  Which (global or local) package installation is
used can also be manipulated by using prefixes and/or using the
&lsquo;<samp class="samp">local_list</samp>&rsquo; input argument.  Using these mechanisms, several different
releases of the same package can be installed side by side as well (but
cannot be loaded simultaneously).
</p>
<p>Packages might depend on external software and/or other packages.  To be
able to install such packages, these dependencies should be installed
beforehand.  A package that depends on other package(s) can still be
installed using the <samp class="option">-nodeps</samp> flag.  The effects of unsatisfied
dependencies on external software&mdash;like libraries&mdash;depends on the
individual package.
</p>
<p>Packages must be loaded before they can be used.  When loading a package,
Octave performs the following tasks:
</p><ol class="enumerate">
<li> If the package depends on other packages (and <code class="code">pkg load</code> is called
without the <samp class="option">-nodeps</samp> option), the package is not loaded
immediately.  Instead, those dependencies are loaded first (recursively if
needed).

</li><li> When all dependencies are satisfied, the package&rsquo;s subdirectories are
added to the search path.
</li></ol>

<p>This load order leads to functions that are provided by dependencies being
potentially shadowed by functions of the same name that are provided by
top-level packages.
</p>
<p>Each time, a package is added to the search path, initialization script(s)
for the package are automatically executed if they are provided by the
package.
</p>
<p>Depending on the value of <var class="var">command</var> and on the number of requested
return arguments, <code class="code">pkg</code> can be used to perform several tasks.
Possible values for <var class="var">command</var> are:
</p>
<dl class="table">
<dt>&lsquo;<samp class="samp">install</samp>&rsquo;</dt>
<dd><p>Install named packages.  For example,
</p>
<div class="example">
<pre class="example-preformatted">pkg install image-1.0.0.tar.gz
</pre></div>

<p>installs the package found in the file <samp class="file">image-1.0.0.tar.gz</samp>.  The
file containing the package can be a URL, e.g.,
</p>
<div class="example">
<pre class="example-preformatted">pkg install 'http://somewebsite.org/image-1.0.0.tar.gz'
</pre></div>

<p>installs the package found in the given URL.  This
requires an internet connection and the cURL library.
</p>
<p><em class="emph">Security risk</em>: no verification of the package is performed
before the installation.  It has the same security issues as manually
downloading the package from the given URL and installing it.
</p>
<p><em class="emph">No support</em>: the GNU Octave community is not responsible for
packages installed from foreign sites.  For support or for
reporting bugs you need to contact the maintainers of the installed
package directly (see the <samp class="file">DESCRIPTION</samp> file of the package)
</p>
<p>The <var class="var">option</var> variable can contain options that affect the manner
in which a package is installed.  These options can be one or more of
</p>
<dl class="table">
<dt><code class="code">-nodeps</code></dt>
<dd><p>The package manager will disable dependency checking.  With this option it
is possible to install a package even when it depends on another package
which is not installed on the system.  <strong class="strong">Use this option with care.</strong>
</p>
</dd>
<dt><code class="code">-local</code></dt>
<dd><p>A local installation (package available only to current user) is forced,
even if Octave is being run with administrative privileges.
</p>
</dd>
<dt><code class="code">-global</code></dt>
<dd><p>A global installation (package available to all users) is forced, even if
Octave is not being run with administrative privileges.  The user must have
write access to the global package store.
</p>
</dd>
<dt><code class="code">-forge</code></dt>
<dd><p>Install a package directly from the Octave Forge repository.  This
requires an internet connection and the cURL library.
</p>
<p><em class="emph">Security risk</em>: no verification of the package is performed
before the installation.  There are no signatures for packages, or
checksums to confirm the correct file was downloaded.  It has the
same security issues as manually downloading the package from the
Octave Forge repository and installing it.
</p>
</dd>
<dt><code class="code">-verbose</code></dt>
<dd><p>The package manager will print the output of all commands as
they are performed.
</p></dd>
</dl>

</dd>
<dt>&lsquo;<samp class="samp">update</samp>&rsquo;</dt>
<dd><p>Check installed Octave Forge packages against repository and update any
outdated items.  Updated packages are installed either globally or locally
depending on whether Octave is running with elevated privileges.  This
requires an internet connection and the cURL library.
</p>
<p>Options for the install command and the names of individual packages to be
checked for updates may be specified as a list following the update
command.  If the <samp class="option">-local</samp> or <samp class="option">-global</samp> option is specified,
<code class="code">pkg update</code> limits the update check to the local or global installed
packages, and installs updates in that same context.  For example,
</p>
<p>Update all packages:
</p>
<div class="example">
<pre class="example-preformatted">pkg update
</pre></div>

<p>Update all local packages:
</p>
<div class="example">
<pre class="example-preformatted">pkg update -local
</pre></div>

<p>Update certain packages, ignore dependencies, max verbosity:
</p>
<div class="example">
<pre class="example-preformatted">pkg update -verbose -nodeps image signal geometry
</pre></div>

<p>Updates for multiple packages are sorted alphabetically and not checked
for dependencies affected by installation order.  If dependency order
related <code class="code">pkg update</code> failure occurs, use <code class="code">pkg update -nodeps</code> to
ignore dependencies, or <code class="code">pkg install -forge &lt;package_name&gt;</code> to update
individual packages manually.
</p>
</dd>
<dt>&lsquo;<samp class="samp">uninstall</samp>&rsquo;</dt>
<dd><p>Uninstall named packages.  For example,
</p>
<div class="example">
<pre class="example-preformatted">pkg uninstall image
</pre></div>

<p>removes the <code class="code">image</code> package from the system.  If another installed
package depends on the <code class="code">image</code> package an error will be issued.
The package can be uninstalled anyway by using the <samp class="option">-nodeps</samp> option.
</p>
</dd>
<dt>&lsquo;<samp class="samp">load</samp>&rsquo;</dt>
<dd><p>Add named packages to the path.  After loading a package it is
possible to use the functions provided by the package.  For example,
</p>
<div class="example">
<pre class="example-preformatted">pkg load image
</pre></div>

<p>adds the <code class="code">image</code> package to the path.
</p>
<p>Note: When loading a package, <code class="code">pkg</code> will automatically try to load
any unloaded dependencies as well, unless the <samp class="option">-nodeps</samp> flag has
been specified.  For example,
</p>
<div class="example">
<pre class="example-preformatted">pkg load signal
</pre></div>

<p>adds the <code class="code">signal</code> package and also tries to load its dependency: the
<code class="code">control</code> package.  Be aware that the functionality of package(s)
loaded will probably be impacted by use of the <samp class="option">-nodeps</samp> flag.  Even
if necessary dependencies are loaded later, the functionality of top-level
packages can still be affected because the optimal loading order may not
have been followed.
</p>
</dd>
<dt>&lsquo;<samp class="samp">unload</samp>&rsquo;</dt>
<dd><p>Remove named packages from the path.  After unloading a package it is
no longer possible to use the functions provided by the package.  Trying
to unload a package that other loaded packages still depend on will result
in an error; no packages will be unloaded in this case.  A package can
be forcibly removed with the <samp class="option">-nodeps</samp> flag, but be aware that the
functionality of dependent packages will likely be affected.  As when
loading packages, reloading dependencies after having unloaded them with the
<samp class="option">-nodeps</samp> flag may not restore all functionality of the dependent
packages as the required loading order may be incorrect.
</p>
</dd>
<dt>&lsquo;<samp class="samp">list</samp>&rsquo;</dt>
<dd><p>Show the list of currently installed packages.  For example,
</p>
<div class="example">
<pre class="example-preformatted">pkg list
</pre></div>

<p>will produce a short report with the package name, version, and installation
directory for each installed package.  Supply a package name to limit
reporting to a particular package.  For example:
</p>
<div class="example">
<pre class="example-preformatted">pkg list image
</pre></div>

<p>If a single return argument is requested then <code class="code">pkg</code> returns a cell
array where each element is a structure with information on a single
package.
</p>
<div class="example">
<pre class="example-preformatted">installed_packages = pkg (&quot;list&quot;)
</pre></div>

<p>If two output arguments are requested <code class="code">pkg</code> splits the list of
installed packages into those which were installed by the current user,
and those which were installed by the system administrator.
</p>
<div class="example">
<pre class="example-preformatted">[user_packages, system_packages] = pkg (&quot;list&quot;)
</pre></div>

<p>The <code class="code">&quot;-forge&quot;</code> option lists packages available at the Octave Forge
repository.  This requires an internet connection and the cURL library.
For example:
</p>
<div class="example">
<pre class="example-preformatted">oct_forge_pkgs = pkg (&quot;list&quot;, &quot;-forge&quot;)
</pre></div>

</dd>
<dt>&lsquo;<samp class="samp">describe</samp>&rsquo;</dt>
<dd><p>Show a short description of installed packages.  With the option
<code class="code">&quot;-verbose&quot;</code> also list functions provided by the package.  For
example,
</p>
<div class="example">
<pre class="example-preformatted">pkg describe -verbose
</pre></div>

<p>will describe all installed packages and the functions they provide.
Display can be limited to a set of packages:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">## describe control and signal packages
pkg describe control signal
</pre></div></div>

<p>If one output is requested a cell of structure containing the
description and list of functions of each package is returned as
output rather than printed on screen:
</p>
<div class="example">
<pre class="example-preformatted">desc = pkg (&quot;describe&quot;, &quot;secs1d&quot;, &quot;image&quot;)
</pre></div>

<p>If any of the requested packages is not installed, <code class="code">pkg</code> returns an
error, unless a second output is requested:
</p>
<div class="example">
<pre class="example-preformatted">[desc, flag] = pkg (&quot;describe&quot;, &quot;secs1d&quot;, &quot;image&quot;)
</pre></div>

<p><var class="var">flag</var> will take one of the values <code class="code">&quot;Not installed&quot;</code>,
<code class="code">&quot;Loaded&quot;</code>, or
<code class="code">&quot;Not loaded&quot;</code> for each of the named packages.
</p>
</dd>
<dt>&lsquo;<samp class="samp">prefix</samp>&rsquo;</dt>
<dd><p>Set the installation prefix directory.  For example,
</p>
<div class="example">
<pre class="example-preformatted">pkg prefix ~/my_octave_packages
</pre></div>

<p>sets the installation prefix to <samp class="file">~/my_octave_packages</samp>.
Packages will be installed in this directory.
</p>
<p>It is possible to get the current installation prefix by requesting an
output argument.  For example:
</p>
<div class="example">
<pre class="example-preformatted">pfx = pkg (&quot;prefix&quot;)
</pre></div>

<p>The location in which to install the architecture dependent files can be
independently specified with an addition argument.  For example:
</p>
<div class="example">
<pre class="example-preformatted">pkg prefix ~/my_octave_packages ~/my_arch_dep_pkgs
</pre></div>

</dd>
<dt>&lsquo;<samp class="samp">local_list</samp>&rsquo;</dt>
<dd><p>Set the file in which to look for information on locally
installed packages.  Locally installed packages are those that are
available only to the current user.  For example:
</p>
<div class="example">
<pre class="example-preformatted">pkg local_list ~/.octave_packages
</pre></div>

<p>It is possible to get the current value of local_list with the following
</p>
<div class="example">
<pre class="example-preformatted">pkg local_list
</pre></div>

</dd>
<dt>&lsquo;<samp class="samp">global_list</samp>&rsquo;</dt>
<dd><p>Set the file in which to look for information on globally
installed packages.  Globally installed packages are those that are
available to all users.  For example:
</p>
<div class="example">
<pre class="example-preformatted">pkg global_list /usr/share/octave/octave_packages
</pre></div>

<p>It is possible to get the current value of global_list with the following
</p>
<div class="example">
<pre class="example-preformatted">pkg global_list
</pre></div>

</dd>
<dt>&lsquo;<samp class="samp">build</samp>&rsquo;</dt>
<dd><p>Build a binary form of a package or packages.  The binary file produced
will itself be an Octave package that can be installed normally with
<code class="code">pkg</code>.  The form of the command to build a binary package is
</p>
<div class="example">
<pre class="example-preformatted">pkg build builddir image-1.0.0.tar.gz ...
</pre></div>

<p>where <code class="code">builddir</code> is the name of a directory where the temporary
installation will be produced and the binary packages will be found.
The options <samp class="option">-verbose</samp> and <samp class="option">-nodeps</samp> are respected, while
all other options are ignored.
</p>
</dd>
<dt>&lsquo;<samp class="samp">rebuild</samp>&rsquo;</dt>
<dd><p>Rebuild the package database from the installed directories.  This can
be used in cases where the package database has been corrupted.
</p>
</dd>
<dt>&lsquo;<samp class="samp">test</samp>&rsquo;</dt>
<dd><p>Perform the built-in self tests contained in all functions provided by
the named packages.  For example:
</p>
<div class="example">
<pre class="example-preformatted">pkg test image
</pre></div>

</dd>
</dl>

<p><strong class="strong">See also:</strong> <a class="ref" href="System-Information.html#XREFver">ver</a>, <a class="ref" href="Getting-Help.html#XREFnews">news</a>.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Using-Packages.html">Using Packages</a>, Up: <a href="Packages.html">Packages</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
