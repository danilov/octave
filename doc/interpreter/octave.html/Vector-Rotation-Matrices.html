<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Vector Rotation Matrices (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Vector Rotation Matrices (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Vector Rotation Matrices (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Geometry.html" rel="up" title="Geometry">
<link href="Interpolation-on-Scattered-Data.html" rel="prev" title="Interpolation on Scattered Data">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="Vector-Rotation-Matrices">
<div class="nav-panel">
<p>
Previous: <a href="Interpolation-on-Scattered-Data.html" accesskey="p" rel="prev">Interpolation on Scattered Data</a>, Up: <a href="Geometry.html" accesskey="u" rel="up">Geometry</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Vector-Rotation-Matrices-1"><span>30.5 Vector Rotation Matrices<a class="copiable-link" href="#Vector-Rotation-Matrices-1"> &para;</a></span></h3>

<p>Also included in Octave&rsquo;s geometry functions are primitive functions to enable
vector rotations in 3-dimensional space.  Separate functions are provided for
rotation about each of the principle axes, <var class="var">x</var>, <var class="var">y</var>, and <var class="var">z</var>.
According to Euler&rsquo;s rotation theorem, any arbitrary rotation, <var class="var">R</var>, of any
vector, <var class="var">p</var>, can be expressed as a product of the three principle
rotations:
</p>

<div class="example">
<pre class="example-preformatted">p' = Rp = Rz*Ry*Rx*p
</pre></div>

<a class="anchor" id="XREFrotx"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-rotx"><span class="category-def">: </span><span><code class="def-type"><var class="var">T</var> =</code> <strong class="def-name">rotx</strong> <code class="def-code-arguments">(<var class="var">angle</var>)</code><a class="copiable-link" href="#index-rotx"> &para;</a></span></dt>
<dd>
<p><code class="code">rotx</code> returns the 3x3 transformation matrix corresponding to an active
rotation of a vector about the x-axis by the specified <var class="var">angle</var>, given in
degrees, where a positive angle corresponds to a counterclockwise
rotation when viewing the y-z plane from the positive x side.
</p>
<p>The form of the transformation matrix is:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">     | 1      0           0      |
 T = | 0  cos(<var class="var">angle</var>) -sin(<var class="var">angle</var>) |
     | 0  sin(<var class="var">angle</var>)  cos(<var class="var">angle</var>) |
</pre></div></div>

<p>This rotation matrix is intended to be used as a left-multiplying matrix
when acting on a column vector, using the notation
<code class="code"><var class="var">v</var> = <var class="var">T</var>*<var class="var">u</var></code>.
For example, a vector, <var class="var">u</var>, pointing along the positive y-axis, rotated
90-degrees about the x-axis, will result in a vector pointing along the
positive z-axis:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">&gt;&gt; u = [0 1 0]'
u =
   0
   1
   0

&gt;&gt; T = rotx (90)
T =
   1.00000   0.00000   0.00000
   0.00000   0.00000  -1.00000
   0.00000   1.00000   0.00000

&gt;&gt; v = T*u
v =
   0.00000
   0.00000
   1.00000
</pre></div></div>


<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFroty">roty</a>, <a class="ref" href="#XREFrotz">rotz</a>.
</p></dd></dl>


<a class="anchor" id="XREFroty"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-roty"><span class="category-def">: </span><span><code class="def-type"><var class="var">T</var> =</code> <strong class="def-name">roty</strong> <code class="def-code-arguments">(<var class="var">angle</var>)</code><a class="copiable-link" href="#index-roty"> &para;</a></span></dt>
<dd>
<p><code class="code">roty</code> returns the 3x3 transformation matrix corresponding to an active
rotation of a vector about the y-axis by the specified <var class="var">angle</var>, given in
degrees, where a positive angle corresponds to a counterclockwise
rotation when viewing the z-x plane from the positive y side.
</p>
<p>The form of the transformation matrix is:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">     |  cos(<var class="var">angle</var>)  0  sin(<var class="var">angle</var>) |
 T = |      0       1      0      |
     | -sin(<var class="var">angle</var>)  0  cos(<var class="var">angle</var>) |
</pre></div></div>

<p>This rotation matrix is intended to be used as a left-multiplying matrix
when acting on a column vector, using the notation
<code class="code"><var class="var">v</var> = <var class="var">T</var>*<var class="var">u</var></code>.
For example, a vector, <var class="var">u</var>, pointing along the positive z-axis, rotated
90-degrees about the y-axis, will result in a vector pointing along the
positive x-axis:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">  &gt;&gt; u = [0 0 1]'
   u =
      0
      0
      1

   &gt;&gt; T = roty (90)
   T =
      0.00000   0.00000   1.00000
      0.00000   1.00000   0.00000
     -1.00000   0.00000   0.00000

   &gt;&gt; v = T*u
   v =
      1.00000
      0.00000
      0.00000
</pre></div></div>


<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFrotx">rotx</a>, <a class="ref" href="#XREFrotz">rotz</a>.
</p></dd></dl>


<a class="anchor" id="XREFrotz"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-rotz"><span class="category-def">: </span><span><code class="def-type"><var class="var">T</var> =</code> <strong class="def-name">rotz</strong> <code class="def-code-arguments">(<var class="var">angle</var>)</code><a class="copiable-link" href="#index-rotz"> &para;</a></span></dt>
<dd>
<p><code class="code">rotz</code> returns the 3x3 transformation matrix corresponding to an active
rotation of a vector about the z-axis by the specified <var class="var">angle</var>, given in
degrees, where a positive angle corresponds to a counterclockwise
rotation when viewing the x-y plane from the positive z side.
</p>
<p>The form of the transformation matrix is:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">     | cos(<var class="var">angle</var>) -sin(<var class="var">angle</var>) 0 |
 T = | sin(<var class="var">angle</var>)  cos(<var class="var">angle</var>) 0 |
     |     0           0      1 |
</pre></div></div>

<p>This rotation matrix is intended to be used as a left-multiplying matrix
when acting on a column vector, using the notation
<code class="code"><var class="var">v</var> = <var class="var">T</var>*<var class="var">u</var></code>.
For example, a vector, <var class="var">u</var>, pointing along the positive x-axis, rotated
90-degrees about the z-axis, will result in a vector pointing along the
positive y-axis:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">  &gt;&gt; u = [1 0 0]'
   u =
      1
      0
      0

   &gt;&gt; T = rotz (90)
   T =
      0.00000  -1.00000   0.00000
      1.00000   0.00000   0.00000
      0.00000   0.00000   1.00000

   &gt;&gt; v = T*u
   v =
      0.00000
      1.00000
      0.00000
</pre></div></div>


<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFrotx">rotx</a>, <a class="ref" href="#XREFroty">roty</a>.
</p></dd></dl>



</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Interpolation-on-Scattered-Data.html">Interpolation on Scattered Data</a>, Up: <a href="Geometry.html">Geometry</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
