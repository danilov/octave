<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Concatenating Strings (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Concatenating Strings (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Concatenating Strings (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="String-Operations.html" rel="up" title="String Operations">
<link href="Splitting-and-Joining-Strings.html" rel="next" title="Splitting and Joining Strings">
<link href="Common-String-Operations.html" rel="prev" title="Common String Operations">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
ul.mark-bullet {list-style-type: disc}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="subsection-level-extent" id="Concatenating-Strings">
<div class="nav-panel">
<p>
Next: <a href="Splitting-and-Joining-Strings.html" accesskey="n" rel="next">Splitting and Joining Strings</a>, Previous: <a href="Common-String-Operations.html" accesskey="p" rel="prev">Common String Operations</a>, Up: <a href="String-Operations.html" accesskey="u" rel="up">String Operations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Concatenating-Strings-1"><span>5.3.2 Concatenating Strings<a class="copiable-link" href="#Concatenating-Strings-1"> &para;</a></span></h4>

<p>Strings can be concatenated using matrix notation
(see <a class="pxref" href="Strings.html">Strings</a>, <a class="ref" href="Character-Arrays.html">Character Arrays</a>) which is often the most natural
method.  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">fullname = [fname &quot;.txt&quot;];
email = [&quot;&lt;&quot; user &quot;@&quot; domain &quot;&gt;&quot;];
</pre></div></div>

<p>In each case it is easy to see what the final string will look like.  This
method is also the most efficient.  When using matrix concatenation the parser
immediately begins joining the strings without having to process
the overhead of a function call and the input validation of the associated
function.
</p>
<p>The <code class="code">newline</code> function can be used to join strings such that they appear
as multiple lines of text when displayed.
</p>
<a class="anchor" id="XREFnewline"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-newline"><span class="category-def">: </span><span><code class="def-type"><var class="var">c</var> =</code> <strong class="def-name">newline</strong><a class="copiable-link" href="#index-newline"> &para;</a></span></dt>
<dd><p>Return the character corresponding to a newline.
</p>
<p>This is equivalent to <code class="code">&quot;\n&quot;</code>.
</p>
<p>Example Code
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">joined_string = [newline &quot;line1&quot; newline &quot;line2&quot;]
&rArr;
line1
line2
</pre></div></div>


<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFstrcat">strcat</a>, <a class="ref" href="Splitting-and-Joining-Strings.html#XREFstrjoin">strjoin</a>, <a class="ref" href="Splitting-and-Joining-Strings.html#XREFstrsplit">strsplit</a>.
</p></dd></dl>


<p>In addition, there are several other functions for concatenating string
objects which can be useful in specific circumstances: <code class="code">char</code>,
<code class="code">strvcat</code>, <code class="code">strcat</code>, and <code class="code">cstrcat</code>.  Finally, the general
purpose concatenation functions can be used: see <a class="ref" href="Rearranging-Matrices.html#XREFcat">cat</a>,
<a class="ref" href="Rearranging-Matrices.html#XREFhorzcat">horzcat</a>, and <a class="ref" href="Rearranging-Matrices.html#XREFvertcat">vertcat</a>.
</p>
<ul class="itemize mark-bullet">
<li>All string concatenation functions except <code class="code">cstrcat</code>
convert numerical input into character data by taking the corresponding UTF-8
character for each element (or multi-byte sequence), as in the following
example:

<div class="example">
<div class="group"><pre class="example-preformatted">char ([98, 97, 110, 97, 110, 97])
   &rArr; banana
</pre></div></div>

<p>For conversion between locale encodings and UTF-8, see
<a class="ref" href="String-encoding.html#XREFunicode2native">unicode2native</a> and
<a class="ref" href="String-encoding.html#XREFnative2unicode">native2unicode</a>.
</p>
</li><li><code class="code">char</code> and <code class="code">strvcat</code>
concatenate vertically, while <code class="code">strcat</code> and <code class="code">cstrcat</code> concatenate
horizontally.  For example:

<div class="example">
<div class="group"><pre class="example-preformatted">char (&quot;an apple&quot;, &quot;two pears&quot;)
    &rArr; an apple
       two pears
</pre></div><pre class="example-preformatted">

</pre><div class="group"><pre class="example-preformatted">strcat (&quot;oc&quot;, &quot;tave&quot;, &quot; is&quot;, &quot; good&quot;, &quot; for you&quot;)
     &rArr; octave is good for you
</pre></div></div>

</li><li><code class="code">char</code> generates an empty row in the output
for each empty string in the input.  <code class="code">strvcat</code>, on the other hand,
eliminates empty strings.

<div class="example">
<div class="group"><pre class="example-preformatted">char (&quot;orange&quot;, &quot;green&quot;, &quot;&quot;, &quot;red&quot;)
    &rArr; orange
       green

       red
</pre></div><pre class="example-preformatted">

</pre><div class="group"><pre class="example-preformatted">strvcat (&quot;orange&quot;, &quot;green&quot;, &quot;&quot;, &quot;red&quot;)
    &rArr; orange
       green
       red
</pre></div></div>

</li><li>All string concatenation functions except <code class="code">cstrcat</code> also accept cell
array data (see <a class="pxref" href="Cell-Arrays.html">Cell Arrays</a>).  <code class="code">char</code> and
<code class="code">strvcat</code> convert cell arrays into character arrays, while <code class="code">strcat</code>
concatenates within the cells of the cell arrays:

<div class="example">
<div class="group"><pre class="example-preformatted">char ({&quot;red&quot;, &quot;green&quot;, &quot;&quot;, &quot;blue&quot;})
     &rArr; red
        green

        blue
</pre></div><pre class="example-preformatted">

</pre><div class="group"><pre class="example-preformatted">strcat ({&quot;abc&quot;; &quot;ghi&quot;}, {&quot;def&quot;; &quot;jkl&quot;})
     &rArr;
        {
          [1,1] = abcdef
          [2,1] = ghijkl
        }
</pre></div></div>

</li><li><code class="code">strcat</code> removes trailing white space in the arguments (except
within cell arrays), while <code class="code">cstrcat</code> leaves white space untouched.  Both
kinds of behavior can be useful as can be seen in the examples:

<div class="example">
<div class="group"><pre class="example-preformatted">strcat ([&quot;dir1&quot;;&quot;directory2&quot;], [&quot;/&quot;;&quot;/&quot;], [&quot;file1&quot;;&quot;file2&quot;])
     &rArr; dir1/file1
        directory2/file2
</pre></div><div class="group"><pre class="example-preformatted">

cstrcat ([&quot;thirteen apples&quot;; &quot;a banana&quot;], [&quot; 5$&quot;;&quot; 1$&quot;])
      &rArr; thirteen apples 5$
         a banana        1$
</pre></div></div>

<p>Note that in the above example for <code class="code">cstrcat</code>, the white space originates
from the internal representation of the strings in a string array
(see <a class="pxref" href="Character-Arrays.html">Character Arrays</a>).
</p></li></ul>

<a class="anchor" id="XREFchar"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-char"><span class="category-def">: </span><span><code class="def-type"><var class="var">C</var> =</code> <strong class="def-name">char</strong> <code class="def-code-arguments">(<var class="var">A</var>)</code><a class="copiable-link" href="#index-char"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-char-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">C</var> =</code> <strong class="def-name">char</strong> <code class="def-code-arguments">(<var class="var">A</var>, &hellip;)</code><a class="copiable-link" href="#index-char-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-char-2"><span class="category-def">: </span><span><code class="def-type"><var class="var">C</var> =</code> <strong class="def-name">char</strong> <code class="def-code-arguments">(<var class="var">str1</var>, <var class="var">str2</var>, &hellip;)</code><a class="copiable-link" href="#index-char-2"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-char-3"><span class="category-def">: </span><span><code class="def-type"><var class="var">C</var> =</code> <strong class="def-name">char</strong> <code class="def-code-arguments">(<var class="var">cell_array</var>)</code><a class="copiable-link" href="#index-char-3"> &para;</a></span></dt>
<dd><p>Create a string array from one or more numeric matrices, character
matrices, or cell arrays.
</p>
<p>Arguments are concatenated vertically.  The returned values are padded with
blanks as needed to make each row of the string array have the same length.
Empty input strings are significant and will concatenated in the output.
</p>
<p>For numerical input, each element is converted to the corresponding ASCII
character.  A range error results if an input is outside the ASCII range
(0-255).
</p>
<p>For cell arrays, each element is concatenated separately.  Cell arrays
converted through <code class="code">char</code> can mostly be converted back with
<code class="code">cellstr</code>.  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">char ([97, 98, 99], &quot;&quot;, {&quot;98&quot;, &quot;99&quot;, 100}, &quot;str1&quot;, [&quot;ha&quot;, &quot;lf&quot;])
   &rArr; [&quot;abc &quot;
       &quot;    &quot;
       &quot;98  &quot;
       &quot;99  &quot;
       &quot;d   &quot;
       &quot;str1&quot;
       &quot;half&quot;]
</pre></div></div>

<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFstrvcat">strvcat</a>, <a class="ref" href="Cell-Arrays-of-Strings.html#XREFcellstr">cellstr</a>.
</p></dd></dl>


<a class="anchor" id="XREFstrvcat"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-strvcat"><span class="category-def">: </span><span><code class="def-type"><var class="var">C</var> =</code> <strong class="def-name">strvcat</strong> <code class="def-code-arguments">(<var class="var">A</var>)</code><a class="copiable-link" href="#index-strvcat"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-strvcat-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">C</var> =</code> <strong class="def-name">strvcat</strong> <code class="def-code-arguments">(<var class="var">A</var>, &hellip;)</code><a class="copiable-link" href="#index-strvcat-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-strvcat-2"><span class="category-def">: </span><span><code class="def-type"><var class="var">C</var> =</code> <strong class="def-name">strvcat</strong> <code class="def-code-arguments">(<var class="var">str1</var>, <var class="var">str2</var>, &hellip;)</code><a class="copiable-link" href="#index-strvcat-2"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-strvcat-3"><span class="category-def">: </span><span><code class="def-type"><var class="var">C</var> =</code> <strong class="def-name">strvcat</strong> <code class="def-code-arguments">(<var class="var">cell_array</var>)</code><a class="copiable-link" href="#index-strvcat-3"> &para;</a></span></dt>
<dd><p>Create a character array from one or more numeric matrices, character
matrices, or cell arrays.
</p>
<p>Arguments are concatenated vertically.  The returned values are padded with
blanks as needed to make each row of the string array have the same length.
Unlike <code class="code">char</code>, empty strings are removed and will not appear in the
output.
</p>
<p>For numerical input, each element is converted to the corresponding ASCII
character.  A range error results if an input is outside the ASCII range
(0-255).
</p>
<p>For cell arrays, each element is concatenated separately.  Cell arrays
converted through <code class="code">strvcat</code> can mostly be converted back with
<code class="code">cellstr</code>.  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">strvcat ([97, 98, 99], &quot;&quot;, {&quot;98&quot;, &quot;99&quot;, 100}, &quot;str1&quot;, [&quot;ha&quot;, &quot;lf&quot;])
      &rArr; [&quot;abc &quot;
          &quot;98  &quot;
          &quot;99  &quot;
          &quot;d   &quot;
          &quot;str1&quot;
          &quot;half&quot;]
</pre></div></div>

<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFchar">char</a>, <a class="ref" href="#XREFstrcat">strcat</a>, <a class="ref" href="#XREFcstrcat">cstrcat</a>.
</p></dd></dl>


<a class="anchor" id="XREFstrcat"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-strcat"><span class="category-def">: </span><span><code class="def-type"><var class="var">str</var> =</code> <strong class="def-name">strcat</strong> <code class="def-code-arguments">(<var class="var">s1</var>, <var class="var">s2</var>, &hellip;)</code><a class="copiable-link" href="#index-strcat"> &para;</a></span></dt>
<dd><p>Return a string containing all the arguments concatenated horizontally.
</p>
<p>If the arguments are cell strings, <code class="code">strcat</code> returns a cell string
with the individual cells concatenated.  For numerical input, each element
is converted to the corresponding ASCII character.  Trailing white space
for any character string input is eliminated before the strings are
concatenated.  Note that cell string values do <strong class="strong">not</strong> have
whitespace trimmed.
</p>
<p>For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">strcat (&quot;|&quot;, &quot; leading space is preserved&quot;, &quot;|&quot;)
    &rArr; | leading space is preserved|
</pre></div></div>

<div class="example">
<div class="group"><pre class="example-preformatted">strcat (&quot;|&quot;, &quot;trailing space is eliminated &quot;, &quot;|&quot;)
    &rArr; |trailing space is eliminated|
</pre></div></div>

<div class="example">
<div class="group"><pre class="example-preformatted">strcat (&quot;homogeneous space |&quot;, &quot;  &quot;, &quot;| is also eliminated&quot;)
    &rArr; homogeneous space || is also eliminated
</pre></div></div>

<div class="example">
<div class="group"><pre class="example-preformatted">s = [ &quot;ab&quot;; &quot;cde&quot; ];
strcat (s, s, s)
    &rArr;
        &quot;ababab   &quot;
        &quot;cdecdecde&quot;
</pre></div></div>

<div class="example">
<div class="group"><pre class="example-preformatted">s = { &quot;ab&quot;; &quot;cd &quot; };
strcat (s, s, s)
    &rArr;
        {
          [1,1] = ababab
          [2,1] = cd cd cd
        }
</pre></div></div>


<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFcstrcat">cstrcat</a>, <a class="ref" href="#XREFchar">char</a>, <a class="ref" href="#XREFstrvcat">strvcat</a>.
</p></dd></dl>


<a class="anchor" id="XREFcstrcat"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-cstrcat"><span class="category-def">: </span><span><code class="def-type"><var class="var">str</var> =</code> <strong class="def-name">cstrcat</strong> <code class="def-code-arguments">(<var class="var">s1</var>, <var class="var">s2</var>, &hellip;)</code><a class="copiable-link" href="#index-cstrcat"> &para;</a></span></dt>
<dd><p>Return a string containing all the arguments concatenated horizontally
with trailing white space preserved.
</p>
<p>For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">cstrcat (&quot;ab   &quot;, &quot;cd&quot;)
      &rArr; &quot;ab   cd&quot;
</pre></div></div>

<div class="example">
<div class="group"><pre class="example-preformatted">s = [ &quot;ab&quot;; &quot;cde&quot; ];
cstrcat (s, s, s)
      &rArr; &quot;ab ab ab &quot;
         &quot;cdecdecde&quot;
</pre></div></div>

<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFstrcat">strcat</a>, <a class="ref" href="#XREFchar">char</a>, <a class="ref" href="#XREFstrvcat">strvcat</a>.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Splitting-and-Joining-Strings.html">Splitting and Joining Strings</a>, Previous: <a href="Common-String-Operations.html">Common String Operations</a>, Up: <a href="String-Operations.html">String Operations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
