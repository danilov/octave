<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Multiple Return Values (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Multiple Return Values (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Multiple Return Values (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Functions-and-Scripts.html" rel="up" title="Functions and Scripts">
<link href="Variable_002dlength-Return-Lists.html" rel="next" title="Variable-length Return Lists">
<link href="Returning-from-a-Function.html" rel="prev" title="Returning from a Function">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="Multiple-Return-Values">
<div class="nav-panel">
<p>
Next: <a href="Variable_002dlength-Return-Lists.html" accesskey="n" rel="next">Variable-length Return Lists</a>, Previous: <a href="Returning-from-a-Function.html" accesskey="p" rel="prev">Returning from a Function</a>, Up: <a href="Functions-and-Scripts.html" accesskey="u" rel="up">Functions and Scripts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Multiple-Return-Values-1"><span>11.4 Multiple Return Values<a class="copiable-link" href="#Multiple-Return-Values-1"> &para;</a></span></h3>

<p>Unlike many other computer languages, Octave allows you to define
functions that return more than one value.  The syntax for defining
functions that return multiple values is
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">function [<var class="var">ret-list</var>] = <var class="var">name</var> (<var class="var">arg-list</var>)
  <var class="var">body</var>
endfunction
</pre></div></div>

<p>where <var class="var">name</var>, <var class="var">arg-list</var>, and <var class="var">body</var> have the same meaning
as before, and <var class="var">ret-list</var> is a comma-separated list of variable
names that will hold the values returned from the function.  The list of
return values must have at least one element.  If <var class="var">ret-list</var> has
only one element, this form of the <code class="code">function</code> statement is
equivalent to the form described in the previous section.
</p>
<p>Here is an example of a function that returns two values, the maximum
element of a vector and the index of its first occurrence in the vector.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">function [max, idx] = vmax (v)
  idx = 1;
  max = v (idx);
  for i = 2:length (v)
    if (v (i) &gt; max)
      max = v (i);
      idx = i;
    endif
  endfor
endfunction
</pre></div></div>

<p>In this particular case, the two values could have been returned as
elements of a single array, but that is not always possible or
convenient.  The values to be returned may not have compatible
dimensions, and it is often desirable to give the individual return
values distinct names.
</p>
<p>It is possible to use the <code class="code">nthargout</code> function to obtain only some
of the return values or several at once in a cell array.
See <a class="xref" href="Cell-Array-Objects.html">Cell Array Objects</a>.
</p>
<a class="anchor" id="XREFnthargout"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-nthargout"><span class="category-def">: </span><span><code class="def-type"><var class="var">arg</var> =</code> <strong class="def-name">nthargout</strong> <code class="def-code-arguments">(<var class="var">n</var>, <var class="var">fcn</var>, &hellip;)</code><a class="copiable-link" href="#index-nthargout"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-nthargout-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">arg</var> =</code> <strong class="def-name">nthargout</strong> <code class="def-code-arguments">(<var class="var">n</var>, <var class="var">ntot</var>, <var class="var">fcn</var>, &hellip;)</code><a class="copiable-link" href="#index-nthargout-1"> &para;</a></span></dt>
<dd><p>Return the <var class="var">n</var>th output argument of the function specified by the
function handle or string <var class="var">fcn</var>.
</p>
<p>Any additional arguments are passed directly to <var class="var">fcn</var>.  The total
number of arguments to call <var class="var">fcn</var> with can be passed in <var class="var">ntot</var>; by
default <var class="var">ntot</var> is <var class="var">n</var>.  The input <var class="var">n</var> can also be a vector of
indices of the output, in which case the output will be a cell array of the
requested output arguments.
</p>
<p>The intended use of <code class="code">nthargout</code> is to avoid intermediate variables.
For example, when finding the indices of the maximum entry of a matrix, the
following two compositions of <code class="code">nthargout</code>
</p>
<div class="example">
<div class="group"><pre class="example-preformatted"><var class="var">m</var> = magic (5);
cell2mat (nthargout ([1, 2], @ind2sub, size (<var class="var">m</var>),
                     nthargout (2, @max, <var class="var">m</var>(:))))
&rArr; 5   3
</pre></div></div>

<p>are completely equivalent to the following lines:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted"><var class="var">m</var> = magic (5);
[~, idx] = max (<var class="var">M</var>(:));
[i, j] = ind2sub (size (<var class="var">m</var>), idx);
[i, j]
&rArr; 5   3
</pre></div></div>

<p>It can also be helpful to have all output arguments collected in a single
cell array as the following code demonstrates:
</p>
<div class="example">
<pre class="example-preformatted"><var class="var">USV</var> = nthargout ([1:3], @svd, hilb (5));
</pre></div>


<p><strong class="strong">See also:</strong> <a class="ref" href="Defining-Functions.html#XREFnargin">nargin</a>, <a class="ref" href="#XREFnargout">nargout</a>, <a class="ref" href="Variable_002dlength-Argument-Lists.html#XREFvarargin">varargin</a>, <a class="ref" href="Variable_002dlength-Return-Lists.html#XREFvarargout">varargout</a>, <a class="ref" href="Ignoring-Arguments.html#XREFisargout">isargout</a>.
</p></dd></dl>


<p>In addition to setting <code class="code">nargin</code> each time a function is called,
Octave also automatically initializes <code class="code">nargout</code> to the number of
values that are expected to be returned.  This allows you to write
functions that behave differently depending on the number of values that
the user of the function has requested.  The implicit assignment to the
built-in variable <code class="code">ans</code> does not figure in the count of output
arguments, so the value of <code class="code">nargout</code> may be zero.
</p>
<p>The <code class="code">svd</code> and <code class="code">hist</code> functions are examples of built-in
functions that behave differently depending on the value of
<code class="code">nargout</code>.  For example, <code class="code">hist</code> will draw a histogram when called
with no output variables, but if called with outputs it will return the
frequency counts and/or bin centers without creating a plot.
</p>
<p>It is possible to write functions that only set some return values.  For
example, calling the function
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">function [x, y, z] = f ()
  x = 1;
  z = 2;
endfunction
</pre></div></div>

<p>as
</p>
<div class="example">
<pre class="example-preformatted">[a, b, c] = f ()
</pre></div>

<p>produces:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">a = 1

b = [](0x0)

c = 2
</pre></div></div>

<p>along with a warning.
</p>
<a class="anchor" id="XREFnargout"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-nargout"><span class="category-def">: </span><span><code class="def-type"><var class="var">n</var> =</code> <strong class="def-name">nargout</strong> <code class="def-code-arguments">()</code><a class="copiable-link" href="#index-nargout"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-nargout-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">n</var> =</code> <strong class="def-name">nargout</strong> <code class="def-code-arguments">(<var class="var">fcn</var>)</code><a class="copiable-link" href="#index-nargout-1"> &para;</a></span></dt>
<dd><p>Report the number of output arguments from a function.
</p>
<p>Called from within a function, return the number of values the caller
expects to receive.  At the top level, <code class="code">nargout</code> with no argument is
undefined and will produce an error.
</p>
<p>If called with the optional argument <var class="var">fcn</var>&mdash;a function name or
handle&mdash;return the number of declared output values that the function can
produce.
</p>
<p>If the final output argument is <var class="var">varargout</var> the returned value is
negative.
</p>
<p>For example,
</p>
<div class="example">
<pre class="example-preformatted">f ()
</pre></div>

<p>will cause <code class="code">nargout</code> to return 0 inside the function <code class="code">f</code> and
</p>
<div class="example">
<pre class="example-preformatted">[s, t] = f ()
</pre></div>

<p>will cause <code class="code">nargout</code> to return 2 inside the function <code class="code">f</code>.
</p>
<p>In the second usage,
</p>
<div class="example">
<pre class="example-preformatted">nargout (@histc)   # or nargout (&quot;histc&quot;) using a string input
</pre></div>

<p>will return 2, because <code class="code">histc</code> has two outputs, whereas
</p>
<div class="example">
<pre class="example-preformatted">nargout (@imread)
</pre></div>

<p>will return -2, because <code class="code">imread</code> has two outputs and the second is
<var class="var">varargout</var>.
</p>
<p>Programming Note.  <code class="code">nargout</code> does not work for built-in functions and
returns -1 for all anonymous functions.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="Defining-Functions.html#XREFnargin">nargin</a>, <a class="ref" href="Variable_002dlength-Return-Lists.html#XREFvarargout">varargout</a>, <a class="ref" href="Ignoring-Arguments.html#XREFisargout">isargout</a>, <a class="ref" href="#XREFnthargout">nthargout</a>.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Variable_002dlength-Return-Lists.html">Variable-length Return Lists</a>, Previous: <a href="Returning-from-a-Function.html">Returning from a Function</a>, Up: <a href="Functions-and-Scripts.html">Functions and Scripts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
