<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Identifying Points in Triangulation (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Identifying Points in Triangulation (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Identifying Points in Triangulation (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Delaunay-Triangulation.html" rel="up" title="Delaunay Triangulation">
<link href="Plotting-the-Triangulation.html" rel="prev" title="Plotting the Triangulation">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="subsection-level-extent" id="Identifying-Points-in-Triangulation">
<div class="nav-panel">
<p>
Previous: <a href="Plotting-the-Triangulation.html" accesskey="p" rel="prev">Plotting the Triangulation</a>, Up: <a href="Delaunay-Triangulation.html" accesskey="u" rel="up">Delaunay Triangulation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Identifying-Points-in-Triangulation-1"><span>30.1.2 Identifying Points in Triangulation<a class="copiable-link" href="#Identifying-Points-in-Triangulation-1"> &para;</a></span></h4>

<p>It is often necessary to identify whether a particular point in the
N-dimensional space is within the Delaunay tessellation of a set of
points in this N-dimensional space, and if so which N-simplex contains
the point and which point in the tessellation is closest to the desired
point.  The functions <code class="code">tsearch</code> and <code class="code">dsearch</code> perform this
function in a triangulation, and <code class="code">tsearchn</code> and <code class="code">dsearchn</code> in
an N-dimensional tessellation.
</p>
<p>To identify whether a particular point represented by a vector <var class="var">p</var>
falls within one of the simplices of an N-simplex, we can write the
Cartesian coordinates of the point in a parametric form with respect to
the N-simplex.  This parametric form is called the Barycentric
Coordinates of the point.  If the points defining the N-simplex are given
by <var class="var">N</var> + 1 vectors <code class="code"><var class="var">t</var>(<var class="var">i</var>,:)</code>, then the Barycentric
coordinates defining the point <var class="var">p</var> are given by
</p>
<div class="example">
<pre class="example-preformatted"><var class="var">p</var> = <var class="var">beta</var> * <var class="var">t</var>
</pre></div>

<p>where <var class="var">beta</var> contains <var class="var">N</var> + 1 values that together as a vector
represent the Barycentric coordinates of the point <var class="var">p</var>.  To ensure a unique
solution for the values of <var class="var">beta</var> an additional criteria of
</p>
<div class="example">
<pre class="example-preformatted">sum (<var class="var">beta</var>) == 1
</pre></div>

<p>is imposed, and we can therefore write the above as
</p>
<div class="example">
<div class="group"><pre class="example-preformatted"><var class="var">p</var> - <var class="var">t</var>(end, :) = <var class="var">beta</var>(1:end-1) * (<var class="var">t</var>(1:end-1, :)
                - ones (<var class="var">N</var>, 1) * <var class="var">t</var>(end, :)
</pre></div></div>

<p>Solving for <var class="var">beta</var> we can then write
</p>
<div class="example">
<div class="group"><pre class="example-preformatted"><var class="var">beta</var>(1:end-1) = (<var class="var">p</var> - <var class="var">t</var>(end, :)) /
                (<var class="var">t</var>(1:end-1, :) - ones (<var class="var">N</var>, 1) * <var class="var">t</var>(end, :))
<var class="var">beta</var>(end) = sum (<var class="var">beta</var>(1:end-1))
</pre></div></div>

<p>which gives the formula for the conversion of the Cartesian coordinates
of the point <var class="var">p</var> to the Barycentric coordinates <var class="var">beta</var>.  An
important property of the Barycentric coordinates is that for all points
in the N-simplex
</p>
<div class="example">
<pre class="example-preformatted">0 &lt;= <var class="var">beta</var>(<var class="var">i</var>) &lt;= 1
</pre></div>

<p>Therefore, the test in <code class="code">tsearch</code> and <code class="code">tsearchn</code> essentially
only needs to express each point in terms of the Barycentric coordinates
of each of the simplices of the N-simplex and test the values of
<var class="var">beta</var>.  This is exactly the implementation used in
<code class="code">tsearchn</code>.  <code class="code">tsearch</code> is optimized for 2-dimensions and the
Barycentric coordinates are not explicitly formed.
</p>
<a class="anchor" id="XREFtsearch"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-tsearch"><span class="category-def">: </span><span><code class="def-type"><var class="var">idx</var> =</code> <strong class="def-name">tsearch</strong> <code class="def-code-arguments">(<var class="var">x</var>, <var class="var">y</var>, <var class="var">t</var>, <var class="var">xi</var>, <var class="var">yi</var>)</code><a class="copiable-link" href="#index-tsearch"> &para;</a></span></dt>
<dd><p>Search for the enclosing Delaunay convex hull.
</p>
<p>For <code class="code"><var class="var">t</var> = delaunay (<var class="var">x</var>, <var class="var">y</var>)</code>, finds the index in <var class="var">t</var>
containing the points <code class="code">(<var class="var">xi</var>, <var class="var">yi</var>)</code>.  For points outside the
convex hull, <var class="var">idx</var> is NaN.
</p>
<p>Programming Note: The algorithm is <code class="code">O</code>(<var class="var">M</var>*<var class="var">N</var>) for locating
<var class="var">M</var> points in <var class="var">N</var> triangles.  Performance is typically much faster if
the points to be located are in a single continuous path; a point is first
checked against the region its predecessor was found in, speeding up lookups
for points along a continuous path.
</p>

<p><strong class="strong">See also:</strong> <a class="ref" href="Delaunay-Triangulation.html#XREFdelaunay">delaunay</a>, <a class="ref" href="Delaunay-Triangulation.html#XREFdelaunayn">delaunayn</a>.
</p></dd></dl>


<a class="anchor" id="XREFtsearchn"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-tsearchn"><span class="category-def">: </span><span><code class="def-type"><var class="var">idx</var> =</code> <strong class="def-name">tsearchn</strong> <code class="def-code-arguments">(<var class="var">x</var>, <var class="var">t</var>, <var class="var">xi</var>)</code><a class="copiable-link" href="#index-tsearchn"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-tsearchn-1"><span class="category-def">: </span><span><code class="def-type">[<var class="var">idx</var>, <var class="var">p</var>] =</code> <strong class="def-name">tsearchn</strong> <code class="def-code-arguments">(<var class="var">x</var>, <var class="var">t</var>, <var class="var">xi</var>)</code><a class="copiable-link" href="#index-tsearchn-1"> &para;</a></span></dt>
<dd><p>Find the simplexes enclosing the given points.
</p>
<p><code class="code">tsearchn</code> is typically used with <code class="code">delaunayn</code>:
<code class="code"><var class="var">t</var> = delaunayn (<var class="var">x</var>)</code> returns a set of simplexes <code class="code">t</code>,
then <code class="code">tsearchn</code> returns the row index of <var class="var">t</var> containing each point
of <var class="var">xi</var>.  For points outside the convex hull, <var class="var">idx</var> is NaN.
</p>
<p>If requested, <code class="code">tsearchn</code> also returns the barycentric coordinates
<var class="var">p</var> of the enclosing simplexes.
</p>

<p><strong class="strong">See also:</strong> <a class="ref" href="Delaunay-Triangulation.html#XREFdelaunay">delaunay</a>, <a class="ref" href="Delaunay-Triangulation.html#XREFdelaunayn">delaunayn</a>, <a class="ref" href="#XREFtsearch">tsearch</a>.
</p></dd></dl>


<p>An example of the use of <code class="code">tsearch</code> can be seen with the simple
triangulation
</p>
<div class="example">
<div class="group"><pre class="example-preformatted"><var class="var">x</var> = [-1; -1; 1; 1];
<var class="var">y</var> = [-1; 1; -1; 1];
<var class="var">tri</var> = [1, 2, 3; 2, 3, 4];
</pre></div></div>

<p>consisting of two triangles defined by <var class="var">tri</var>.  We can then identify
which triangle a point falls in like
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">tsearch (<var class="var">x</var>, <var class="var">y</var>, <var class="var">tri</var>, -0.5, -0.5)
&rArr; 1
tsearch (<var class="var">x</var>, <var class="var">y</var>, <var class="var">tri</var>, 0.5, 0.5)
&rArr; 2
</pre></div></div>

<p>and we can confirm that a point doesn&rsquo;t lie within one of the triangles like
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">tsearch (<var class="var">x</var>, <var class="var">y</var>, <var class="var">tri</var>, 2, 2)
&rArr; NaN
</pre></div></div>

<p>The <code class="code">dsearch</code> and <code class="code">dsearchn</code> find the closest point in a
tessellation to the desired point.  The desired point does not
necessarily have to be in the tessellation, and even if it the returned
point of the tessellation does not have to be one of the vertices of the
N-simplex within which the desired point is found.
</p>
<a class="anchor" id="XREFdsearch"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-dsearch"><span class="category-def">: </span><span><code class="def-type"><var class="var">idx</var> =</code> <strong class="def-name">dsearch</strong> <code class="def-code-arguments">(<var class="var">x</var>, <var class="var">y</var>, <var class="var">tri</var>, <var class="var">xi</var>, <var class="var">yi</var>)</code><a class="copiable-link" href="#index-dsearch"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-dsearch-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">idx</var> =</code> <strong class="def-name">dsearch</strong> <code class="def-code-arguments">(<var class="var">x</var>, <var class="var">y</var>, <var class="var">tri</var>, <var class="var">xi</var>, <var class="var">yi</var>, <var class="var">s</var>)</code><a class="copiable-link" href="#index-dsearch-1"> &para;</a></span></dt>
<dd><p>Return the index <var class="var">idx</var> of the closest point in <code class="code"><var class="var">x</var>, <var class="var">y</var></code>
to the elements <code class="code">[<var class="var">xi</var>(:), <var class="var">yi</var>(:)]</code>.
</p>
<p>The variable <var class="var">s</var> is accepted for compatibility but is ignored.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFdsearchn">dsearchn</a>, <a class="ref" href="#XREFtsearch">tsearch</a>.
</p></dd></dl>


<a class="anchor" id="XREFdsearchn"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-dsearchn"><span class="category-def">: </span><span><code class="def-type"><var class="var">idx</var> =</code> <strong class="def-name">dsearchn</strong> <code class="def-code-arguments">(<var class="var">x</var>, <var class="var">tri</var>, <var class="var">xi</var>)</code><a class="copiable-link" href="#index-dsearchn"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-dsearchn-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">idx</var> =</code> <strong class="def-name">dsearchn</strong> <code class="def-code-arguments">(<var class="var">x</var>, <var class="var">tri</var>, <var class="var">xi</var>, <var class="var">outval</var>)</code><a class="copiable-link" href="#index-dsearchn-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-dsearchn-2"><span class="category-def">: </span><span><code class="def-type"><var class="var">idx</var> =</code> <strong class="def-name">dsearchn</strong> <code class="def-code-arguments">(<var class="var">x</var>, <var class="var">xi</var>)</code><a class="copiable-link" href="#index-dsearchn-2"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-dsearchn-3"><span class="category-def">: </span><span><code class="def-type">[<var class="var">idx</var>, <var class="var">d</var>] =</code> <strong class="def-name">dsearchn</strong> <code class="def-code-arguments">(&hellip;)</code><a class="copiable-link" href="#index-dsearchn-3"> &para;</a></span></dt>
<dd><p>Return the index <var class="var">idx</var> of the closest point in <var class="var">x</var> to the elements
<var class="var">xi</var>.
</p>
<p>If <var class="var">outval</var> is supplied, then the values of <var class="var">xi</var> that are not
contained within one of the simplices <var class="var">tri</var> are set to <var class="var">outval</var>.
Generally, <var class="var">tri</var> is returned from <code class="code">delaunayn (<var class="var">x</var>)</code>.
</p>
<p>The optional output <var class="var">d</var> contains a column vector of distances between
the query points <var class="var">xi</var> and the nearest simplex points <var class="var">x</var>.
</p>

<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFdsearch">dsearch</a>, <a class="ref" href="#XREFtsearch">tsearch</a>.
</p></dd></dl>


<p>An example of the use of <code class="code">dsearch</code>, using the above values of
<var class="var">x</var>, <var class="var">y</var> and <var class="var">tri</var> is
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">dsearch (<var class="var">x</var>, <var class="var">y</var>, <var class="var">tri</var>, -2, -2)
&rArr; 1
</pre></div></div>

<p>If you wish the points that are outside the tessellation to be flagged,
then <code class="code">dsearchn</code> can be used as
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">dsearchn ([<var class="var">x</var>, <var class="var">y</var>], <var class="var">tri</var>, [-2, -2], NaN)
&rArr; NaN
dsearchn ([<var class="var">x</var>, <var class="var">y</var>], <var class="var">tri</var>, [-0.5, -0.5], NaN)
&rArr; 1
</pre></div></div>

<p>where the point outside the tessellation are then flagged with <code class="code">NaN</code>.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Plotting-the-Triangulation.html">Plotting the Triangulation</a>, Up: <a href="Delaunay-Triangulation.html">Delaunay Triangulation</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
