<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Audio Data Processing (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Audio Data Processing (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Audio Data Processing (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Audio-Processing.html" rel="up" title="Audio Processing">
<link href="Audio-Recorder.html" rel="prev" title="Audio Recorder">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="Audio-Data-Processing">
<div class="nav-panel">
<p>
Previous: <a href="Audio-Recorder.html" accesskey="p" rel="prev">Audio Recorder</a>, Up: <a href="Audio-Processing.html" accesskey="u" rel="up">Audio Processing</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Audio-Data-Processing-1"><span>33.5 Audio Data Processing<a class="copiable-link" href="#Audio-Data-Processing-1"> &para;</a></span></h3>

<p>Octave provides a few functions for dealing with audio data.  An audio
&lsquo;sample&rsquo; is a single output value from an A/D converter, i.e., a small
integer number (usually 8 or 16 bits), and audio data is just a series
of such samples.  It can be characterized by three parameters: the
sampling rate (measured in samples per second or Hz, e.g., 8000 or
44100), the number of bits per sample (e.g., 8 or 16), and the number of
channels (1 for mono, 2 for stereo, etc.).
</p>
<p>There are many different formats for representing such data.  Currently,
only the two most popular, <em class="emph">linear encoding</em> and <em class="emph">mu-law
encoding</em>, are supported by Octave.  There is an excellent FAQ on audio
formats by Guido van Rossum <a class="email" href="mailto:guido@cwi.nl">guido@cwi.nl</a> which can be
found at any FAQ ftp site, in particular in the directory
<samp class="file">/pub/usenet/news.answers/audio-fmts</samp> of the archive site
<code class="code">rtfm.mit.edu</code>.
</p>
<p>Octave simply treats audio data as vectors of samples (non-mono data are
not supported yet).  It is assumed that audio files using linear
encoding have one of the extensions <samp class="file">lin</samp> or <samp class="file">raw</samp>, and that
files holding data in mu-law encoding end in <samp class="file">au</samp>, <samp class="file">mu</samp>, or
<samp class="file">snd</samp>.
</p>
<a class="anchor" id="XREFlin2mu"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-lin2mu"><span class="category-def">: </span><span><code class="def-type"><var class="var">y</var> =</code> <strong class="def-name">lin2mu</strong> <code class="def-code-arguments">(<var class="var">x</var>)</code><a class="copiable-link" href="#index-lin2mu"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-lin2mu-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">y</var> =</code> <strong class="def-name">lin2mu</strong> <code class="def-code-arguments">(<var class="var">x</var>, <var class="var">n</var>)</code><a class="copiable-link" href="#index-lin2mu-1"> &para;</a></span></dt>
<dd><p>Convert audio data from linear to mu-law.
</p>
<p>Linear values use floating point values in the range -1 &le; <var class="var">x</var>
&le; 1 if <var class="var">n</var> is 0 (default), or <var class="var">n</var>-bit signed integers if <var class="var">n</var>
is 8 or 16.  Mu-law values are 8-bit unsigned integers in the range
0 &le; <var class="var">y</var> &le; 255.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFmu2lin">mu2lin</a>.
</p></dd></dl>


<a class="anchor" id="XREFmu2lin"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-mu2lin"><span class="category-def">: </span><span><code class="def-type"><var class="var">y</var> =</code> <strong class="def-name">mu2lin</strong> <code class="def-code-arguments">(<var class="var">x</var>)</code><a class="copiable-link" href="#index-mu2lin"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-mu2lin-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">y</var> =</code> <strong class="def-name">mu2lin</strong> <code class="def-code-arguments">(<var class="var">x</var>, <var class="var">n</var>)</code><a class="copiable-link" href="#index-mu2lin-1"> &para;</a></span></dt>
<dd><p>Convert audio data from mu-law to linear.
</p>
<p>Mu-law values are 8-bit unsigned integers in the range 0 &le; <var class="var">y</var>
&le; 255.  Linear values use floating point values in the range
-<var class="var">linmax</var> &le; <var class="var">x</var> <var class="var">linmax</var> (where
<code class="code"><var class="var">linmax</var> = 32124/32768 =~ 0.98</code>) when <var class="var">n</var> is zero (default).
If <var class="var">n</var> is 8 or 16 then <var class="var">n</var>-bit signed integers are used instead.
</p>
<p>Programming Note: <code class="code">mu2lin</code> maps maximum mu-law inputs to values
slightly below the maximum ([-0.98, +0.98]) representable with a linear
scale.  Because of this, <code class="code">mu2lin (lin2mu (<var class="var">x</var>))</code> might not
reproduce the original input.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFlin2mu">lin2mu</a>.
</p></dd></dl>


<a class="anchor" id="XREFrecord"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-record-2"><span class="category-def">: </span><span><code class="def-type"><var class="var">data</var> =</code> <strong class="def-name">record</strong> <code class="def-code-arguments">(<var class="var">sec</var>)</code><a class="copiable-link" href="#index-record-2"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-record-3"><span class="category-def">: </span><span><code class="def-type"><var class="var">data</var> =</code> <strong class="def-name">record</strong> <code class="def-code-arguments">(<var class="var">sec</var>, <var class="var">fs</var>)</code><a class="copiable-link" href="#index-record-3"> &para;</a></span></dt>
<dd><p>Record <var class="var">sec</var> seconds of audio from the system&rsquo;s default audio input at
a sampling rate of 8000 samples per second.
</p>
<p>If the optional argument <var class="var">fs</var> is given, it specifies the sampling rate
for recording.
</p>
<p>For more control over audio recording, use the <code class="code">audiorecorder</code> class.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="Audio-Recorder.html#XREF_0040audiorecorder_002faudiorecorder">@audiorecorder/audiorecorder</a>, <a class="ref" href="#XREFsound">sound</a>, <a class="ref" href="#XREFsoundsc">soundsc</a>.
</p></dd></dl>


<a class="anchor" id="XREFsound"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-sound"><span class="category-def">: </span><span><strong class="def-name">sound</strong> <code class="def-code-arguments">(<var class="var">y</var>)</code><a class="copiable-link" href="#index-sound"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-sound-1"><span class="category-def">: </span><span><strong class="def-name">sound</strong> <code class="def-code-arguments">(<var class="var">y</var>, <var class="var">fs</var>)</code><a class="copiable-link" href="#index-sound-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-sound-2"><span class="category-def">: </span><span><strong class="def-name">sound</strong> <code class="def-code-arguments">(<var class="var">y</var>, <var class="var">fs</var>, <var class="var">nbits</var>)</code><a class="copiable-link" href="#index-sound-2"> &para;</a></span></dt>
<dd><p>Play audio data <var class="var">y</var> at sample rate <var class="var">fs</var> to the default audio
device.
</p>
<p>The audio signal <var class="var">y</var> can be a vector or a two-column array representing
mono or stereo audio, respectively.
</p>
<p>If <var class="var">fs</var> is not given, a default sample rate of 8000 samples per second
is used.
</p>
<p>The optional argument <var class="var">nbits</var> specifies the bit depth to play to the
audio device and defaults to 8 bits.
</p>
<p>For more control over audio playback, use the <code class="code">audioplayer</code> class.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFsoundsc">soundsc</a>, <a class="ref" href="Audio-Player.html#XREF_0040audioplayer_002faudioplayer">@audioplayer/audioplayer</a>, <a class="ref" href="#XREFrecord">record</a>.
</p></dd></dl>


<a class="anchor" id="XREFsoundsc"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-soundsc"><span class="category-def">: </span><span><strong class="def-name">soundsc</strong> <code class="def-code-arguments">(<var class="var">y</var>)</code><a class="copiable-link" href="#index-soundsc"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-soundsc-1"><span class="category-def">: </span><span><strong class="def-name">soundsc</strong> <code class="def-code-arguments">(<var class="var">y</var>, <var class="var">fs</var>)</code><a class="copiable-link" href="#index-soundsc-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-soundsc-2"><span class="category-def">: </span><span><strong class="def-name">soundsc</strong> <code class="def-code-arguments">(<var class="var">y</var>, <var class="var">fs</var>, <var class="var">nbits</var>)</code><a class="copiable-link" href="#index-soundsc-2"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-soundsc-3"><span class="category-def">: </span><span><strong class="def-name">soundsc</strong> <code class="def-code-arguments">(&hellip;, [<var class="var">ymin</var>, <var class="var">ymax</var>])</code><a class="copiable-link" href="#index-soundsc-3"> &para;</a></span></dt>
<dd><p>Scale the audio data <var class="var">y</var> and play it at sample rate <var class="var">fs</var> to the
default audio device.
</p>
<p>The audio signal <var class="var">y</var> can be a vector or a two-column array representing
mono or stereo audio, respectively.
</p>
<p>If <var class="var">fs</var> is not given, a default sample rate of 8000 samples per second
is used.
</p>
<p>The optional argument <var class="var">nbits</var> specifies the bit depth to play to the
audio device and defaults to 8 bits.
</p>
<p>By default, <var class="var">y</var> is automatically normalized to the range [-1, 1].  If
the range [<var class="var">ymin</var>, <var class="var">ymax</var>] is given, then elements of <var class="var">y</var>
that fall within the range <var class="var">ymin</var> &le; <var class="var">y</var> &le; <var class="var">ymax</var>
are scaled to the range [-1, 1] instead.
</p>
<p>For more control over audio playback, use the <code class="code">audioplayer</code> class.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFsound">sound</a>, <a class="ref" href="Audio-Player.html#XREF_0040audioplayer_002faudioplayer">@audioplayer/audioplayer</a>, <a class="ref" href="#XREFrecord">record</a>.
</p></dd></dl>




</div>
<hr>
<div class="nav-panel">
<p>
Previous: <a href="Audio-Recorder.html">Audio Recorder</a>, Up: <a href="Audio-Processing.html">Audio Processing</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
