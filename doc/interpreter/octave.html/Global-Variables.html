<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Global Variables (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Global Variables (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Global Variables (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Variables.html" rel="up" title="Variables">
<link href="Persistent-Variables.html" rel="next" title="Persistent Variables">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="Global-Variables">
<div class="nav-panel">
<p>
Next: <a href="Persistent-Variables.html" accesskey="n" rel="next">Persistent Variables</a>, Up: <a href="Variables.html" accesskey="u" rel="up">Variables</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Global-Variables-1"><span>7.1 Global Variables<a class="copiable-link" href="#Global-Variables-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-global-variables"></a>
<a class="index-entry-id" id="index-global-statement"></a>
<a class="index-entry-id" id="index-variables_002c-global"></a>

<p>See keyword:  <a class="ref" href="Keywords.html#XREFglobal">global</a>
</p>
<p>A <em class="dfn">global</em> variable is one that may be accessed anywhere within Octave.
This is in contrast to a local variable which can only be accessed outside
of its current context if it is passed explicitly, such as by including it as a
parameter when calling a function
(<code class="code">fcn (<var class="var">local_var1</var>, <var class="var">local_var2</var>)</code>).
</p>
<p>A variable is declared global by using a <code class="code">global</code> declaration statement.
The following statements are all global declarations.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">global a
global a b
global c = 2
global d = 3 e f = 5
</pre></div></div>

<p>Note that the <code class="code">global</code> qualifier extends only to the next end-of-statement
indicator which could be a comma (&lsquo;<samp class="samp">,</samp>&rsquo;), semicolon (&lsquo;<samp class="samp">;</samp>&rsquo;), or newline
(&lsquo;<samp class="samp">'\n'</samp>&rsquo;).  For example, the following code declares one global variable,
<var class="var">a</var>, and one local variable <var class="var">b</var> to which the value 1 is assigned.
</p>
<div class="example">
<pre class="example-preformatted">global a, b = 1
</pre></div>

<p>A global variable may only be initialized once in a <code class="code">global</code> statement.
For example, after executing the following code
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">global gvar = 1
global gvar = 2
</pre></div></div>

<p>the value of the global variable <code class="code">gvar</code> is 1, not 2.  Issuing a
&lsquo;<samp class="samp">clear gvar</samp>&rsquo; command does not change the above behavior, but
&lsquo;<samp class="samp">clear all</samp>&rsquo; does.
</p>
<p>It is necessary declare a variable as global within a function body in order to
access the one universal variable.  For example,
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">global x
function f ()
  x = 1;
endfunction
f ()
</pre></div></div>

<p>does <em class="emph">not</em> set the value of the global variable <code class="code">x</code> to 1.  Instead,
a local variable, with name <code class="code">x</code>, is created and assigned the value of 1.
In order to change the value of the global variable <code class="code">x</code>, you must also
declare it to be global within the function body, like this
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">function f ()
  global x;
  x = 1;
endfunction
</pre></div></div>

<p>Passing a global variable in a function parameter list will make a local copy
and <em class="emph">not</em> modify the global value.  For example, given the function
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">function f (x)
  x = 0
endfunction
</pre></div></div>

<p>and the definition of <code class="code">x</code> as a global variable at the top level,
</p>
<div class="example">
<pre class="example-preformatted">global x = 13
</pre></div>

<p>the expression
</p>
<div class="example">
<pre class="example-preformatted">f (x)
</pre></div>

<p>will display the value of <code class="code">x</code> from inside the function as 0, but the value
of <code class="code">x</code> at the top level remains unchanged, because the function works with
a <em class="emph">copy</em> of its argument.
</p>
<p>Programming Note: While global variables occasionally are the right solution to
a coding problem, modern best practice discourages their use.  Code which
relies on global variables may behave unpredictably between different users
and can be difficult to debug.  This is because global variables can introduce
systemic changes so that localizing a bug to a particular function, or to a
particular loop within a function, becomes difficult.
</p>
<a class="anchor" id="XREFisglobal"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-isglobal"><span class="category-def">: </span><span><code class="def-type"><var class="var">tf</var> =</code> <strong class="def-name">isglobal</strong> <code class="def-code-arguments">(<var class="var">name</var>)</code><a class="copiable-link" href="#index-isglobal"> &para;</a></span></dt>
<dd><p>Return true if <var class="var">name</var> is a globally visible variable.
</p>
<p>For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">global x
isglobal (&quot;x&quot;)
   &rArr; 1
</pre></div></div>

<p><strong class="strong">See also:</strong> <a class="ref" href="Variables.html#XREFisvarname">isvarname</a>, <a class="ref" href="Status-of-Variables.html#XREFexist">exist</a>.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Persistent-Variables.html">Persistent Variables</a>, Up: <a href="Variables.html">Variables</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
