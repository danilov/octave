<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>The if Statement (GNU Octave (version 9.2.0))</title>

<meta name="description" content="The if Statement (GNU Octave (version 9.2.0))">
<meta name="keywords" content="The if Statement (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Statements.html" rel="up" title="Statements">
<link href="The-switch-Statement.html" rel="next" title="The switch Statement">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="The-if-Statement">
<div class="nav-panel">
<p>
Next: <a href="The-switch-Statement.html" accesskey="n" rel="next">The switch Statement</a>, Up: <a href="Statements.html" accesskey="u" rel="up">Statements</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="The-if-Statement-1"><span>10.1 The if Statement<a class="copiable-link" href="#The-if-Statement-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-if-statement"></a>
<a class="index-entry-id" id="index-else-statement"></a>
<a class="index-entry-id" id="index-elseif-statement"></a>
<a class="index-entry-id" id="index-endif-statement"></a>

<p>The <code class="code">if</code> statement is Octave&rsquo;s decision-making statement.  There
are three basic forms of an <code class="code">if</code> statement.  In its simplest form,
it looks like this:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">if (<var class="var">condition</var>)
  <var class="var">then-body</var>
endif
</pre></div></div>

<p><var class="var">condition</var> is an expression that controls what the rest of the
statement will do.  The <var class="var">then-body</var> is executed only if
<var class="var">condition</var> is true.
</p>
<p>The condition in an <code class="code">if</code> statement is considered true if its value
is nonzero, and false if its value is zero.  If the value of the
conditional expression in an <code class="code">if</code> statement is a vector or a
matrix, it is considered true only if it is non-empty and <em class="emph">all</em>
of the elements are nonzero.  The conceptually equivalent code when
<var class="var">condition</var> is a matrix is shown below.
</p>
<div class="example">
<pre class="example-preformatted">if (<var class="var">matrix</var>) &equiv; if (all (<var class="var">matrix</var>(:)))
</pre></div>

<p>The second form of an if statement looks like this:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">if (<var class="var">condition</var>)
  <var class="var">then-body</var>
else
  <var class="var">else-body</var>
endif
</pre></div></div>

<p>If <var class="var">condition</var> is true, <var class="var">then-body</var> is executed; otherwise,
<var class="var">else-body</var> is executed.
</p>
<p>Here is an example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">if (rem (x, 2) == 0)
  printf (&quot;x is even\n&quot;);
else
  printf (&quot;x is odd\n&quot;);
endif
</pre></div></div>

<p>In this example, if the expression <code class="code">rem (x, 2) == 0</code> is true (that
is, the value of <code class="code">x</code> is divisible by 2), then the first
<code class="code">printf</code> statement is evaluated, otherwise the second <code class="code">printf</code>
statement is evaluated.
</p>
<p>The third and most general form of the <code class="code">if</code> statement allows
multiple decisions to be combined in a single statement.  It looks like
this:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">if (<var class="var">condition</var>)
  <var class="var">then-body</var>
elseif (<var class="var">condition</var>)
  <var class="var">elseif-body</var>
else
  <var class="var">else-body</var>
endif
</pre></div></div>

<p>Any number of <code class="code">elseif</code> clauses may appear.  Each condition is
tested in turn, and if one is found to be true, its corresponding
<var class="var">body</var> is executed.  If none of the conditions are true and the
<code class="code">else</code> clause is present, its body is executed.  Only one
<code class="code">else</code> clause may appear, and it must be the last part of the
statement.
</p>
<p>In the following example, if the first condition is true (that is, the
value of <code class="code">x</code> is divisible by 2), then the first <code class="code">printf</code>
statement is executed.  If it is false, then the second condition is
tested, and if it is true (that is, the value of <code class="code">x</code> is divisible
by 3), then the second <code class="code">printf</code> statement is executed.  Otherwise,
the third <code class="code">printf</code> statement is performed.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">if (rem (x, 2) == 0)
  printf (&quot;x is even\n&quot;);
elseif (rem (x, 3) == 0)
  printf (&quot;x is odd and divisible by 3\n&quot;);
else
  printf (&quot;x is odd\n&quot;);
endif
</pre></div></div>

<p>Note that the <code class="code">elseif</code> keyword must not be spelled <code class="code">else if</code>,
as is allowed in Fortran.  If it is, the space between the <code class="code">else</code>
and <code class="code">if</code> will tell Octave to treat this as a new <code class="code">if</code>
statement within another <code class="code">if</code> statement&rsquo;s <code class="code">else</code> clause.  For
example, if you write
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">if (<var class="var">c1</var>)
  <var class="var">body-1</var>
else if (<var class="var">c2</var>)
  <var class="var">body-2</var>
endif
</pre></div></div>

<p>Octave will expect additional input to complete the first <code class="code">if</code>
statement.  If you are using Octave interactively, it will continue to
prompt you for additional input.  If Octave is reading this input from a
file, it may complain about missing or mismatched <code class="code">end</code> statements,
or, if you have not used the more specific <code class="code">end</code> statements
(<code class="code">endif</code>, <code class="code">endfor</code>, etc.), it may simply produce incorrect
results, without producing any warning messages.
</p>
<p>It is much easier to see the error if we rewrite the statements above
like this,
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">if (<var class="var">c1</var>)
  <var class="var">body-1</var>
else
  if (<var class="var">c2</var>)
    <var class="var">body-2</var>
  endif
</pre></div></div>

<p>using the indentation to show how Octave groups the statements.
See <a class="xref" href="Functions-and-Scripts.html">Functions and Scripts</a>.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="The-switch-Statement.html">The switch Statement</a>, Up: <a href="Statements.html">Statements</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
