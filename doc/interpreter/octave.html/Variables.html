<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Variables (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Variables (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Variables (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="Expressions.html" rel="next" title="Expressions">
<link href="Data-Containers.html" rel="prev" title="Data Containers">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="chapter-level-extent" id="Variables">
<div class="nav-panel">
<p>
Next: <a href="Expressions.html" accesskey="n" rel="next">Expressions</a>, Previous: <a href="Data-Containers.html" accesskey="p" rel="prev">Data Containers</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h2 class="chapter" id="Variables-1"><span>7 Variables<a class="copiable-link" href="#Variables-1"> &para;</a></span></h2>
<a class="index-entry-id" id="index-variables_002c-user_002ddefined"></a>
<a class="index-entry-id" id="index-user_002ddefined-variables"></a>

<p>Variables let you give names to values and refer to them later.  You have
already seen variables in many of the examples.  The name of a variable
must be a sequence of letters, digits and underscores, but it may not begin
with a digit.  Octave does not enforce a limit on the length of variable
names, but it is seldom useful to have variables with names longer than
about 30 characters.  The following are all valid variable names
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">x
x15
__foo_bar_baz__
fucnrdthsucngtagdjb
</pre></div></div>

<p>However, names like <code class="code">__foo_bar_baz__</code> that begin and end with two
underscores are understood to be reserved for internal use by Octave.
You should not use them in code you write, except to access Octave&rsquo;s
documented internal variables and built-in symbolic constants.
</p>
<p>Case is significant in variable names.  The symbols <code class="code">a</code> and
<code class="code">A</code> are distinct variables.
</p>
<p>A variable name is a valid expression by itself.  It represents the
variable&rsquo;s current value.  Variables are given new values with
<em class="dfn">assignment operators</em> and <em class="dfn">increment operators</em>.
See <a class="xref" href="Assignment-Ops.html">Assignment Expressions</a>.
</p>
<p>There is one automatically created variable with a special meaning.  The
<code class="code">ans</code> variable always contains the result of the last computation,
where the output wasn&rsquo;t assigned to any variable.  The code <code class="code">a =
cos (pi)</code> will assign the value -1 to the variable <code class="code">a</code>, but will
not change the value of <code class="code">ans</code>.  However, the code <code class="code">cos (pi)</code>
will set the value of <code class="code">ans</code> to -1.
</p>
<p>Variables in Octave do not have fixed types, so it is possible to first
store a numeric value in a variable and then to later use the same name
to hold a string value in the same program.  Variables may not be used
before they have been given a value.  Doing so results in an error.
</p>
<a class="index-entry-id" id="index-ans"></a>
<a class="anchor" id="XREFans"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-defvr">
<dt class="defvr" id="index-ans-1"><span class="category-def">Automatic Variable: </span><span><strong class="def-name">ans</strong><a class="copiable-link" href="#index-ans-1"> &para;</a></span></dt>
<dd><p>The most recently computed result that was not explicitly assigned to a
variable.
</p>
<p>For example, after the expression
</p>
<div class="example">
<pre class="example-preformatted">3^2 + 4^2
</pre></div>

<p>is evaluated, the value returned by <code class="code">ans</code> is 25.
</p></dd></dl>


<a class="anchor" id="XREFisvarname"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-isvarname"><span class="category-def">: </span><span><code class="def-type"><var class="var">tf</var> =</code> <strong class="def-name">isvarname</strong> <code class="def-code-arguments">(<var class="var">name</var>)</code><a class="copiable-link" href="#index-isvarname"> &para;</a></span></dt>
<dd><p>Return true if <var class="var">name</var> is a valid variable name.
</p>
<p>A valid variable name is composed of letters, digits, and underscores (&quot;_&quot;),
and the first character must not be a digit.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="Keywords.html#XREFiskeyword">iskeyword</a>, <a class="ref" href="Status-of-Variables.html#XREFexist">exist</a>, <a class="ref" href="Status-of-Variables.html#XREFwho">who</a>.
</p></dd></dl>


<a class="anchor" id="XREFmatlab_005flang_005fmakeValidName"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-matlab_002elang_002emakeValidName"><span class="category-def">: </span><span><code class="def-type"><var class="var">varname</var> =</code> <strong class="def-name">matlab.lang.makeValidName</strong> <code class="def-code-arguments">(<var class="var">str</var>)</code><a class="copiable-link" href="#index-matlab_002elang_002emakeValidName"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-matlab_002elang_002emakeValidName-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">varname</var> =</code> <strong class="def-name">matlab.lang.makeValidName</strong> <code class="def-code-arguments">(&hellip;, <code class="code">&quot;ReplacementStyle&quot;</code>, <var class="var">rs</var>)</code><a class="copiable-link" href="#index-matlab_002elang_002emakeValidName-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-matlab_002elang_002emakeValidName-2"><span class="category-def">: </span><span><code class="def-type"><var class="var">varname</var> =</code> <strong class="def-name">matlab.lang.makeValidName</strong> <code class="def-code-arguments">(&hellip;, <code class="code">&quot;Prefix&quot;</code>, <var class="var">pfx</var>)</code><a class="copiable-link" href="#index-matlab_002elang_002emakeValidName-2"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-matlab_002elang_002emakeValidName-3"><span class="category-def">: </span><span><code class="def-type">[<var class="var">varname</var>, <var class="var">ismodified</var>] =</code> <strong class="def-name">matlab.lang.makeValidName</strong> <code class="def-code-arguments">(&hellip;)</code><a class="copiable-link" href="#index-matlab_002elang_002emakeValidName-3"> &para;</a></span></dt>
<dd>
<p>Create valid variable name <var class="var">varname</var> from <var class="var">str</var>.
</p>
<p>The input <var class="var">str</var> must be a string or a cell array of strings.
The output <var class="var">varname</var> will be of the same type.
</p>
<p>A valid variable name is a sequence of letters, digits, and underscores that
does not begin with a digit.
</p>
<p>The <code class="code">&quot;ReplacementStyle&quot;</code> option specifies how invalid characters
are handled.  Acceptable values are
</p>
<dl class="table">
<dt><code class="code">&quot;underscore&quot;</code> (default)</dt>
<dd><p>Replace all invalid characters with an underscore (<code class="code">&quot;_&quot;</code>).
</p>
</dd>
<dt><code class="code">&quot;delete&quot;</code></dt>
<dd><p>Remove any invalid character.
</p>
</dd>
<dt><code class="code">&quot;hex&quot;</code></dt>
<dd><p>Replace all invalid characters with their hexadecimal representation.
</p></dd>
</dl>

<p>Whitespace characters are always removed <strong class="strong">prior</strong> to the application
of the <code class="code">&quot;ReplacementStyle&quot;</code>.  Lowercase letters following a whitespace
will be changed to uppercase.
</p>
<p>The <code class="code">&quot;Prefix&quot;</code> option specifies the string <var class="var">pfx</var> to add as a
prefix to the input if it begins with a digit.  <var class="var">pfx</var> must be a valid
variable name itself.  The default prefix is <code class="code">&quot;x&quot;</code>.
</p>
<p>The optional output <var class="var">ismodified</var> is a logical array indicating whether
the respective element in <var class="var">str</var> was a valid name or not.
</p>

<p><strong class="strong">See also:</strong> <a class="ref" href="Keywords.html#XREFiskeyword">iskeyword</a>, <a class="ref" href="#XREFisvarname">isvarname</a>, <a class="ref" href="#XREFmatlab_005flang_005fmakeUniqueStrings">matlab.lang.makeUniqueStrings</a>.
</p></dd></dl>


<a class="anchor" id="XREFmatlab_005flang_005fmakeUniqueStrings"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-matlab_002elang_002emakeUniqueStrings"><span class="category-def">: </span><span><code class="def-type"><var class="var">uniqstr</var> =</code> <strong class="def-name">matlab.lang.makeUniqueStrings</strong> <code class="def-code-arguments">(<var class="var">str</var>)</code><a class="copiable-link" href="#index-matlab_002elang_002emakeUniqueStrings"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-matlab_002elang_002emakeUniqueStrings-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">uniqstr</var> =</code> <strong class="def-name">matlab.lang.makeUniqueStrings</strong> <code class="def-code-arguments">(<var class="var">str</var>, <var class="var">ex</var>)</code><a class="copiable-link" href="#index-matlab_002elang_002emakeUniqueStrings-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-matlab_002elang_002emakeUniqueStrings-2"><span class="category-def">: </span><span><code class="def-type"><var class="var">uniqstr</var> =</code> <strong class="def-name">matlab.lang.makeUniqueStrings</strong> <code class="def-code-arguments">(<var class="var">str</var>, <var class="var">ex</var>, <var class="var">maxlength</var>)</code><a class="copiable-link" href="#index-matlab_002elang_002emakeUniqueStrings-2"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-matlab_002elang_002emakeUniqueStrings-3"><span class="category-def">: </span><span><code class="def-type">[<var class="var">uniqstr</var>, <var class="var">ismodified</var>] =</code> <strong class="def-name">matlab.lang.makeUniqueStrings</strong> <code class="def-code-arguments">(&hellip;)</code><a class="copiable-link" href="#index-matlab_002elang_002emakeUniqueStrings-3"> &para;</a></span></dt>
<dd>
<p>Construct a list of unique strings from a list of strings.
</p>
<p>The input <var class="var">str</var> must be a string or a cell array of strings.
The output <var class="var">uniqstr</var> will be of the same type.
</p>
<p>The algorithm makes two strings unique by appending an underscore
(<code class="code">&quot;_&quot;</code> and a numeric count to the second string.
</p>
<p>If <var class="var">ex</var> is a string or a cell array of strings, <var class="var">uniqstr</var> will
contain elements that are unique between themselves and with respect to
<var class="var">ex</var>.
</p>
<p>If <var class="var">ex</var> is an index array or a logical array for <var class="var">str</var> then it
selects the subset of <var class="var">str</var> that are made unique.  Unselected elements
are not modified.
</p>
<p>The optional input <var class="var">maxlength</var> specifies the maximum length of any
string in <var class="var">uniqstr</var>.  If an input string cannot be made unique without
exceeding <var class="var">maxlength</var> an error is emitted.
</p>
<p>The optional output <var class="var">ismodified</var> is a logical array indicating whether
each element in <var class="var">str</var> was modified to make it unique.
</p>

<p><strong class="strong">See also:</strong> <a class="ref" href="Sets.html#XREFunique">unique</a>, <a class="ref" href="#XREFmatlab_005flang_005fmakeValidName">matlab.lang.makeValidName</a>.
</p></dd></dl>


<a class="anchor" id="XREFnamelengthmax"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-namelengthmax"><span class="category-def">: </span><span><code class="def-type"><var class="var">n</var> =</code> <strong class="def-name">namelengthmax</strong> <code class="def-code-arguments">()</code><a class="copiable-link" href="#index-namelengthmax"> &para;</a></span></dt>
<dd><p>Return the <small class="sc">MATLAB</small> compatible maximum variable name length.
</p>
<p>Octave is capable of storing strings up to <em class="math">2^{31} - 1</em> in length.
However for <small class="sc">MATLAB</small> compatibility all variable, function, and structure
field names should be shorter than the length returned by
<code class="code">namelengthmax</code>.  In particular, variables stored to a <small class="sc">MATLAB</small> file
format (<samp class="file">*.mat</samp>) will have their names truncated to this length.
</p></dd></dl>



<ul class="mini-toc">
<li><a href="Global-Variables.html" accesskey="1">Global Variables</a></li>
<li><a href="Persistent-Variables.html" accesskey="2">Persistent Variables</a></li>
<li><a href="Status-of-Variables.html" accesskey="3">Status of Variables</a></li>
</ul>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Expressions.html">Expressions</a>, Previous: <a href="Data-Containers.html">Data Containers</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
