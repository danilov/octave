<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Evaluating Polynomials (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Evaluating Polynomials (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Evaluating Polynomials (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Polynomial-Manipulations.html" rel="up" title="Polynomial Manipulations">
<link href="Finding-Roots.html" rel="next" title="Finding Roots">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="Evaluating-Polynomials">
<div class="nav-panel">
<p>
Next: <a href="Finding-Roots.html" accesskey="n" rel="next">Finding Roots</a>, Up: <a href="Polynomial-Manipulations.html" accesskey="u" rel="up">Polynomial Manipulations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Evaluating-Polynomials-1"><span>28.1 Evaluating Polynomials<a class="copiable-link" href="#Evaluating-Polynomials-1"> &para;</a></span></h3>

<p>The value of a polynomial represented by the vector <var class="var">c</var> can be evaluated
at the point <var class="var">x</var> very easily, as the following example shows:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">N = length (c) - 1;
val = dot (x.^(N:-1:0), c);
</pre></div></div>

<p>While the above example shows how easy it is to compute the value of a
polynomial, it isn&rsquo;t the most stable algorithm.  With larger polynomials
you should use more elegant algorithms, such as Horner&rsquo;s Method,
which is exactly what the Octave function <code class="code">polyval</code> does.
</p>
<p>In the case where <var class="var">x</var> is a square matrix, the polynomial given by
<var class="var">c</var> is still well-defined.  As when <var class="var">x</var> is a scalar the obvious
implementation is easily expressed in Octave, but also in this case
more elegant algorithms perform better.  The <code class="code">polyvalm</code> function
provides such an algorithm.
</p>
<a class="anchor" id="XREFpolyval"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-polyval"><span class="category-def">: </span><span><code class="def-type"><var class="var">y</var> =</code> <strong class="def-name">polyval</strong> <code class="def-code-arguments">(<var class="var">p</var>, <var class="var">x</var>)</code><a class="copiable-link" href="#index-polyval"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-polyval-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">y</var> =</code> <strong class="def-name">polyval</strong> <code class="def-code-arguments">(<var class="var">p</var>, <var class="var">x</var>, [], <var class="var">mu</var>)</code><a class="copiable-link" href="#index-polyval-1"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-polyval-2"><span class="category-def">: </span><span><code class="def-type">[<var class="var">y</var>, <var class="var">dy</var>] =</code> <strong class="def-name">polyval</strong> <code class="def-code-arguments">(<var class="var">p</var>, <var class="var">x</var>, <var class="var">s</var>)</code><a class="copiable-link" href="#index-polyval-2"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-polyval-3"><span class="category-def">: </span><span><code class="def-type">[<var class="var">y</var>, <var class="var">dy</var>] =</code> <strong class="def-name">polyval</strong> <code class="def-code-arguments">(<var class="var">p</var>, <var class="var">x</var>, <var class="var">s</var>, <var class="var">mu</var>)</code><a class="copiable-link" href="#index-polyval-3"> &para;</a></span></dt>
<dd>
<p>Evaluate the polynomial <var class="var">p</var> at the specified values of <var class="var">x</var>.
</p>
<p>If <var class="var">x</var> is a vector or matrix, the polynomial is evaluated for each of
the elements of <var class="var">x</var>.
</p>
<p>When <var class="var">mu</var> is present, evaluate the polynomial for
(<var class="var">x</var>&nbsp;-&nbsp;<var class="var">mu</var>(1))&nbsp;/&nbsp;<var class="var">mu</var>(2)<!-- /@w -->.
</p>
<p>In addition to evaluating the polynomial, the second output represents the
prediction interval, <var class="var">y</var> +/- <var class="var">dy</var>, which contains at least 50% of
the future predictions.  To calculate the prediction interval, the
structured variable <var class="var">s</var>, originating from <code class="code">polyfit</code>, must be
supplied.
</p>

<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFpolyvalm">polyvalm</a>, <a class="ref" href="Derivatives-_002f-Integrals-_002f-Transforms.html#XREFpolyaffine">polyaffine</a>, <a class="ref" href="Polynomial-Interpolation.html#XREFpolyfit">polyfit</a>, <a class="ref" href="Finding-Roots.html#XREFroots">roots</a>, <a class="ref" href="Miscellaneous-Functions.html#XREFpoly">poly</a>.
</p></dd></dl>


<a class="anchor" id="XREFpolyvalm"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-polyvalm"><span class="category-def">: </span><span><code class="def-type"><var class="var">y</var> =</code> <strong class="def-name">polyvalm</strong> <code class="def-code-arguments">(<var class="var">c</var>, <var class="var">x</var>)</code><a class="copiable-link" href="#index-polyvalm"> &para;</a></span></dt>
<dd><p>Evaluate a polynomial in the matrix sense.
</p>
<p><code class="code">polyvalm (<var class="var">c</var>, <var class="var">x</var>)</code> will evaluate the polynomial in the
matrix sense, i.e., matrix multiplication is used instead of element by
element multiplication as used in <code class="code">polyval</code>.
</p>
<p>The argument <var class="var">x</var> must be a square matrix.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="#XREFpolyval">polyval</a>, <a class="ref" href="Finding-Roots.html#XREFroots">roots</a>, <a class="ref" href="Miscellaneous-Functions.html#XREFpoly">poly</a>.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Finding-Roots.html">Finding Roots</a>, Up: <a href="Polynomial-Manipulations.html">Polynomial Manipulations</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
