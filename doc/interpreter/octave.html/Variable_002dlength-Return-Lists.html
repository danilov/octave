<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Variable-length Return Lists (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Variable-length Return Lists (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Variable-length Return Lists (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Functions-and-Scripts.html" rel="up" title="Functions and Scripts">
<link href="Variable_002dlength-Argument-Lists.html" rel="next" title="Variable-length Argument Lists">
<link href="Multiple-Return-Values.html" rel="prev" title="Multiple Return Values">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="Variable_002dlength-Return-Lists">
<div class="nav-panel">
<p>
Next: <a href="Variable_002dlength-Argument-Lists.html" accesskey="n" rel="next">Variable-length Argument Lists</a>, Previous: <a href="Multiple-Return-Values.html" accesskey="p" rel="prev">Multiple Return Values</a>, Up: <a href="Functions-and-Scripts.html" accesskey="u" rel="up">Functions and Scripts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Variable_002dlength-Return-Lists-1"><span>11.5 Variable-length Return Lists<a class="copiable-link" href="#Variable_002dlength-Return-Lists-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-variable_002dlength-return-lists"></a>
<a class="index-entry-id" id="index-varargout"></a>
<a class="anchor" id="XREFvarargout"></a>
<p>It is possible to return a variable number of output arguments from a
function using a syntax that&rsquo;s similar to the one used with the
special <code class="code">varargin</code> parameter name.  To let a function return a
variable number of output arguments the special output parameter name
<code class="code">varargout</code> is used.  As with <code class="code">varargin</code>, <code class="code">varargout</code> is
a cell array that will contain the requested output arguments.
</p>
<p>As an example the following function sets the first output argument to
1, the second to 2, and so on.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">function varargout = one_to_n ()
  for i = 1:nargout
    varargout{i} = i;
  endfor
endfunction
</pre></div></div>

<p>When called this function returns values like this
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">[a, b, c] = one_to_n ()
     &rArr; a =  1
     &rArr; b =  2
     &rArr; c =  3
</pre></div></div>

<p>If <code class="code">varargin</code> (<code class="code">varargout</code>) does not appear as the last
element of the input (output) parameter list, then it is not special,
and is handled the same as any other parameter name.
</p>
<a class="anchor" id="XREFdeal"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-deal"><span class="category-def">: </span><span><code class="def-type">[<var class="var">r1</var>, <var class="var">r2</var>, &hellip;, <var class="var">rn</var>] =</code> <strong class="def-name">deal</strong> <code class="def-code-arguments">(<var class="var">a</var>)</code><a class="copiable-link" href="#index-deal"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-deal-1"><span class="category-def">: </span><span><code class="def-type">[<var class="var">r1</var>, <var class="var">r2</var>, &hellip;, <var class="var">rn</var>] =</code> <strong class="def-name">deal</strong> <code class="def-code-arguments">(<var class="var">a1</var>, <var class="var">a2</var>, &hellip;, <var class="var">an</var>)</code><a class="copiable-link" href="#index-deal-1"> &para;</a></span></dt>
<dd>
<p>Copy the input parameters into the corresponding output parameters.
</p>
<p>If only a single input parameter is supplied, its value is copied to each
of the outputs.
</p>
<p>For example,
</p>
<div class="example">
<pre class="example-preformatted">[a, b, c] = deal (x, y, z);
</pre></div>

<p>is equivalent to
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">a = x;
b = y;
c = z;
</pre></div></div>

<p>and
</p>
<div class="example">
<pre class="example-preformatted">[a, b, c] = deal (x);
</pre></div>

<p>is equivalent to
</p>
<div class="example">
<pre class="example-preformatted">a = b = c = x;
</pre></div>

<p>Programming Note: <code class="code">deal</code> is often used with comma-separated lists
derived from cell arrays or structures.  This is unnecessary as the
interpreter can perform the same action without the overhead of a function
call.  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">c = {[1 2], &quot;Three&quot;, 4};
[x, y, z] = c{:}
&rArr;
   x =

      1   2

   y = Three
   z =  4
</pre></div></div>

<p><strong class="strong">See also:</strong> <a class="ref" href="Processing-Data-in-Cell-Arrays.html#XREFcell2struct">cell2struct</a>, <a class="ref" href="Processing-Data-in-Structures.html#XREFstruct2cell">struct2cell</a>, <a class="ref" href="Special-Utility-Matrices.html#XREFrepmat">repmat</a>.
</p></dd></dl>


</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Variable_002dlength-Argument-Lists.html">Variable-length Argument Lists</a>, Previous: <a href="Multiple-Return-Values.html">Multiple Return Values</a>, Up: <a href="Functions-and-Scripts.html">Functions and Scripts</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
