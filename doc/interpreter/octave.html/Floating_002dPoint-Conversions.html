<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Floating-Point Conversions (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Floating-Point Conversions (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Floating-Point Conversions (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="C_002dStyle-I_002fO-Functions.html" rel="up" title="C-Style I/O Functions">
<link href="Other-Output-Conversions.html" rel="next" title="Other Output Conversions">
<link href="Integer-Conversions.html" rel="prev" title="Integer Conversions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
span:hover a.copiable-link {visibility: visible}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="subsection-level-extent" id="Floating_002dPoint-Conversions">
<div class="nav-panel">
<p>
Next: <a href="Other-Output-Conversions.html" accesskey="n" rel="next">Other Output Conversions</a>, Previous: <a href="Integer-Conversions.html" accesskey="p" rel="prev">Integer Conversions</a>, Up: <a href="C_002dStyle-I_002fO-Functions.html" accesskey="u" rel="up">C-Style I/O Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Floating_002dPoint-Conversions-1"><span>14.2.9 Floating-Point Conversions<a class="copiable-link" href="#Floating_002dPoint-Conversions-1"> &para;</a></span></h4>

<p>This section discusses the conversion specifications for floating-point
numbers: the &lsquo;<samp class="samp">%f</samp>&rsquo;, &lsquo;<samp class="samp">%e</samp>&rsquo;, &lsquo;<samp class="samp">%E</samp>&rsquo;, &lsquo;<samp class="samp">%g</samp>&rsquo;, and &lsquo;<samp class="samp">%G</samp>&rsquo;
conversions.
</p>
<p>The &lsquo;<samp class="samp">%f</samp>&rsquo; conversion prints its argument in fixed-point notation,
producing output of the form
[<code class="code">-</code>]<var class="var">ddd</var><code class="code">.</code><var class="var">ddd</var><!-- /@w -->,
where the number of digits following the decimal point is controlled
by the precision you specify.
</p>
<p>The &lsquo;<samp class="samp">%e</samp>&rsquo; conversion prints its argument in exponential notation,
producing output of the form
[<code class="code">-</code>]<var class="var">d</var><code class="code">.</code><var class="var">ddd</var><code class="code">e</code>[<code class="code">+</code>|<code class="code">-</code>]<var class="var">dd</var><!-- /@w -->.
Again, the number of digits following the decimal point is controlled by
the precision.  The exponent always contains at least two digits.  The
&lsquo;<samp class="samp">%E</samp>&rsquo; conversion is similar but the exponent is marked with the letter
&lsquo;<samp class="samp">E</samp>&rsquo; instead of &lsquo;<samp class="samp">e</samp>&rsquo;.
</p>
<p>The &lsquo;<samp class="samp">%g</samp>&rsquo; and &lsquo;<samp class="samp">%G</samp>&rsquo; conversions print the argument in the style
of &lsquo;<samp class="samp">%e</samp>&rsquo; or &lsquo;<samp class="samp">%E</samp>&rsquo; (respectively) if the exponent would be less
than -4 or greater than or equal to the precision; otherwise they use the
&lsquo;<samp class="samp">%f</samp>&rsquo; style.  Trailing zeros are removed from the fractional portion
of the result and a decimal-point character appears only if it is
followed by a digit.
</p>
<p>The following flags can be used to modify the behavior:
</p>
<dl class="table">
<dt>&lsquo;<samp class="samp">-</samp>&rsquo;</dt>
<dd><p>Left-justify the result in the field.  Normally the result is
right-justified.
</p>
</dd>
<dt>&lsquo;<samp class="samp">+</samp>&rsquo;</dt>
<dd><p>Always include a plus or minus sign in the result.
</p>
</dd>
<dt>&lsquo;<samp class="samp"> </samp>&rsquo;</dt>
<dd><p>If the result doesn&rsquo;t start with a plus or minus sign, prefix it with a
space instead.  Since the &lsquo;<samp class="samp">+</samp>&rsquo; flag ensures that the result includes
a sign, this flag is ignored if you supply both of them.
</p>
</dd>
<dt>&lsquo;<samp class="samp">#</samp>&rsquo;</dt>
<dd><p>Specifies that the result should always include a decimal point, even
if no digits follow it.  For the &lsquo;<samp class="samp">%g</samp>&rsquo; and &lsquo;<samp class="samp">%G</samp>&rsquo; conversions,
this also forces trailing zeros after the decimal point to be left
in place where they would otherwise be removed.
</p>
</dd>
<dt>&lsquo;<samp class="samp">0</samp>&rsquo;</dt>
<dd><p>Pad the field with zeros instead of spaces; the zeros are placed
after any sign.  This flag is ignored if the &lsquo;<samp class="samp">-</samp>&rsquo; flag is also
specified.
</p></dd>
</dl>

<p>The precision specifies how many digits follow the decimal-point
character for the &lsquo;<samp class="samp">%f</samp>&rsquo;, &lsquo;<samp class="samp">%e</samp>&rsquo;, and &lsquo;<samp class="samp">%E</samp>&rsquo; conversions.  For
these conversions, the default precision is <code class="code">6</code>.  If the precision
is explicitly <code class="code">0</code>, this suppresses the decimal point character
entirely.  For the &lsquo;<samp class="samp">%g</samp>&rsquo; and &lsquo;<samp class="samp">%G</samp>&rsquo; conversions, the precision
specifies how many significant digits to print.  Significant digits are
the first digit before the decimal point, and all the digits after it.
If the precision is <code class="code">0</code> or not specified for &lsquo;<samp class="samp">%g</samp>&rsquo; or
&lsquo;<samp class="samp">%G</samp>&rsquo;, it is treated like a value of <code class="code">1</code>.  If the value being
printed cannot be expressed precisely in the specified number of digits,
the value is rounded to the nearest number that fits.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Other-Output-Conversions.html">Other Output Conversions</a>, Previous: <a href="Integer-Conversions.html">Integer Conversions</a>, Up: <a href="C_002dStyle-I_002fO-Functions.html">C-Style I/O Functions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
