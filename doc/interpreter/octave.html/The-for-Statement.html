<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>The for Statement (GNU Octave (version 9.2.0))</title>

<meta name="description" content="The for Statement (GNU Octave (version 9.2.0))">
<meta name="keywords" content="The for Statement (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Statements.html" rel="up" title="Statements">
<link href="The-break-Statement.html" rel="next" title="The break Statement">
<link href="The-do_002duntil-Statement.html" rel="prev" title="The do-until Statement">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="The-for-Statement">
<div class="nav-panel">
<p>
Next: <a href="The-break-Statement.html" accesskey="n" rel="next">The break Statement</a>, Previous: <a href="The-do_002duntil-Statement.html" accesskey="p" rel="prev">The do-until Statement</a>, Up: <a href="Statements.html" accesskey="u" rel="up">Statements</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="The-for-Statement-1"><span>10.5 The for Statement<a class="copiable-link" href="#The-for-Statement-1"> &para;</a></span></h3>
<a class="index-entry-id" id="index-for-statement"></a>
<a class="index-entry-id" id="index-endfor-statement"></a>

<p>The <code class="code">for</code> statement makes it more convenient to count iterations of a
loop.  The general form of the <code class="code">for</code> statement looks like this:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">for <var class="var">var</var> = <var class="var">expression</var>
  <var class="var">body</var>
endfor
</pre></div></div>

<p>where <var class="var">body</var> stands for any statement or list of statements,
<var class="var">expression</var> is any valid expression, and <var class="var">var</var> may take several
forms.  Usually it is a simple variable name or an indexed variable.  If
the value of <var class="var">expression</var> is a structure, <var class="var">var</var> may also be a
vector with two elements.  See <a class="xref" href="Looping-Over-Structure-Elements.html">Looping Over Structure Elements</a>, below.
</p>
<p>The assignment expression in the <code class="code">for</code> statement works a bit
differently than Octave&rsquo;s normal assignment statement.  Instead of
assigning the complete result of the expression, it assigns each column
of the expression to <var class="var">var</var> in turn.  If <var class="var">expression</var> is a range,
a row vector, or a scalar, the value of <var class="var">var</var> will be a scalar each
time the loop body is executed.  If <var class="var">var</var> is a column vector or a
matrix, <var class="var">var</var> will be a column vector each time the loop body is
executed.
</p>
<p>The following example shows another way to create a vector containing
the first ten elements of the Fibonacci sequence, this time using the
<code class="code">for</code> statement:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">fib = ones (1, 10);
for i = 3:10
  fib(i) = fib(i-1) + fib(i-2);
endfor
</pre></div></div>

<p>This code works by first evaluating the expression <code class="code">3:10</code>, to
produce a range of values from 3 to 10 inclusive.  Then the variable
<code class="code">i</code> is assigned the first element of the range and the body of the
loop is executed once.  When the end of the loop body is reached, the
next value in the range is assigned to the variable <code class="code">i</code>, and the
loop body is executed again.  This process continues until there are no
more elements to assign.
</p>
<p>Within Octave is it also possible to iterate over matrices or cell arrays
using the <code class="code">for</code> statement.  For example consider
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">disp (&quot;Loop over a matrix&quot;)
for i = [1,3;2,4]
  i
endfor
disp (&quot;Loop over a cell array&quot;)
for i = {1,&quot;two&quot;;&quot;three&quot;,4}
  i
endfor
</pre></div></div>

<p>In this case the variable <code class="code">i</code> takes on the value of the columns of
the matrix or cell matrix.  So the first loop iterates twice, producing
two column vectors <code class="code">[1;2]</code>, followed by <code class="code">[3;4]</code>, and likewise
for the loop over the cell array.  This can be extended to loops over
multi-dimensional arrays.  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">a = [1,3;2,4]; c = cat (3, a, 2*a);
for i = c
  i
endfor
</pre></div></div>

<p>In the above case, the multi-dimensional matrix <var class="var">c</var> is reshaped to a
two-dimensional matrix as <code class="code">reshape (c, rows (c), prod (size (c)(2:end)))</code>
and then the same behavior as a loop over a two-dimensional matrix is produced.
</p>
<p>Although it is possible to rewrite all <code class="code">for</code> loops as <code class="code">while</code>
loops, the Octave language has both statements because often a
<code class="code">for</code> loop is both less work to type and more natural to think of.
Counting the number of iterations is very common in loops and it can be
easier to think of this counting as part of looping rather than as
something to do inside the loop.
</p>

<ul class="mini-toc">
<li><a href="Looping-Over-Structure-Elements.html" accesskey="1">Looping Over Structure Elements</a></li>
</ul>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="The-break-Statement.html">The break Statement</a>, Previous: <a href="The-do_002duntil-Statement.html">The do-until Statement</a>, Up: <a href="Statements.html">Statements</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
