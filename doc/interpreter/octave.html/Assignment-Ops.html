<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Assignment Ops (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Assignment Ops (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Assignment Ops (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Expressions.html" rel="up" title="Expressions">
<link href="Increment-Ops.html" rel="next" title="Increment Ops">
<link href="Boolean-Expressions.html" rel="prev" title="Boolean Expressions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="Assignment-Ops">
<div class="nav-panel">
<p>
Next: <a href="Increment-Ops.html" accesskey="n" rel="next">Increment Operators</a>, Previous: <a href="Boolean-Expressions.html" accesskey="p" rel="prev">Boolean Expressions</a>, Up: <a href="Expressions.html" accesskey="u" rel="up">Expressions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Assignment-Expressions"><span>8.6 Assignment Expressions<a class="copiable-link" href="#Assignment-Expressions"> &para;</a></span></h3>
<a class="index-entry-id" id="index-assignment-expressions"></a>
<a class="index-entry-id" id="index-assignment-operators"></a>
<a class="index-entry-id" id="index-operators_002c-assignment"></a>
<a class="index-entry-id" id="index-expressions_002c-assignment"></a>

<a class="index-entry-id" id="index-_003d"></a>

<p>An <em class="dfn">assignment</em> is an expression that stores a new value into a
variable.  For example, the following expression assigns the value 1 to
the variable <code class="code">z</code>:
</p>
<div class="example">
<pre class="example-preformatted">z = 1
</pre></div>

<p>After this expression is executed, the variable <code class="code">z</code> has the value 1.
Whatever old value <code class="code">z</code> had before the assignment is forgotten.
The &lsquo;<samp class="samp">=</samp>&rsquo; sign is called an <em class="dfn">assignment operator</em>.
</p>
<p>Assignments can store string values also.  For example, the following
expression would store the value <code class="code">&quot;this food is good&quot;</code> in the
variable <code class="code">message</code>:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">thing = &quot;food&quot;
predicate = &quot;good&quot;
message = [ &quot;this &quot; , thing , &quot; is &quot; , predicate ]
&rArr; &quot;this food is good&quot;
</pre></div></div>

<p>(This also illustrates concatenation of strings.)
</p>
<a class="index-entry-id" id="index-side-effect"></a>
<p>Most operators (addition, concatenation, and so on) have no effect
except to compute a value.  If you ignore the value, you might as well
not use the operator.  An assignment operator is different.  It does
produce a value, but even if you ignore the value, the assignment still
makes itself felt through the alteration of the variable.  We call this
a <em class="dfn">side effect</em>.
</p>
<a class="index-entry-id" id="index-lvalue"></a>
<p>The left-hand operand of an assignment need not be a variable
(see <a class="pxref" href="Variables.html">Variables</a>).  It can also be an element of a matrix
(see <a class="pxref" href="Index-Expressions.html">Index Expressions</a>) or a list of return values
(see <a class="pxref" href="Calling-Functions.html">Calling Functions</a>).  These are all called <em class="dfn">lvalues</em>, which
means they can appear on the left-hand side of an assignment operator.
The right-hand operand may be any expression.  It produces the new value
which the assignment stores in the specified variable, matrix element,
or list of return values.
</p>
<p>It is important to note that variables do <em class="emph">not</em> have permanent types.
The type of a variable is simply the type of whatever value it happens
to hold at the moment.  In the following program fragment, the variable
<code class="code">foo</code> has a numeric value at first, and a string value later on:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">&gt;&gt; foo = 1
foo = 1
&gt;&gt; foo = &quot;bar&quot;
foo = bar
</pre></div></div>

<p>When the second assignment gives <code class="code">foo</code> a string value, the fact that
it previously had a numeric value is forgotten.
</p>
<p>Assignment of a scalar to an indexed matrix sets all of the elements
that are referenced by the indices to the scalar value.  For example, if
<code class="code">a</code> is a matrix with at least two columns,
</p>
<div class="example">
<pre class="example-preformatted">a(:, 2) = 5
</pre></div>

<p>sets all the elements in the second column of <code class="code">a</code> to 5.
</p>
<p>When an assignment sets the value of a vector, matrix, or array element at a
position or dimension outside of that variable&rsquo;s current size, the array size
will be increased to accommodate the new values:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">&gt;&gt; a = [1, 2, 3]
a = 1 2 3
&gt;&gt; a(4) = 4
a = 1 2 3 4
&gt;&gt; a(2, :) = [5, 6, 7, 8]
a =
   1   2   3   4
   5   6   7   8
</pre></div></div>

<p>Attempting to increase the size of an array such that the desired output size
is ambiguous will result in an error:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">&gt;&gt; a(9) = 10
-| error: Invalid resizing operation or ambiguous assignment to an
out-of-bounds array element
</pre></div></div>

<p>This is because adding the 9th element creates an ambiguity in the desired
array position for the value 10, each possibility requiring a different array
size expansion to accommodate the assignment.
</p>
<p>Assignments may be made with fewer specified elements than would be required to
fill the newly expanded array as long as the assignment is unambiguous.  In
these cases the array will be automatically padded with <em class="emph">null</em> values:
</p><div class="example">
<div class="group"><pre class="example-preformatted">&gt;&gt; a = [1, 2]
a =   1   2
&gt;&gt; a(4) = 5
a =   1   2   0   5
&gt;&gt; a(3, :) = [6, 7, 8, 9]
a =
   1   2   0   5
   0   0   0   0
   6   7   8   9
&gt;&gt; a(4, 5) = 10
a =
    1    2    0    5    0
    0    0    0    0    0
    6    7    8    9    0
    0    0    0    0   10
</pre></div></div>

<p>For all built-in types, the <em class="emph">null</em> value will be appropriate to that object
type.
</p>
<p>Numeric arrays:
</p><div class="example">
<div class="group"><pre class="example-preformatted">&gt;&gt; a = int32 ([1, 2])
a = 1, 2
&gt;&gt; a(4) = 5
a = 1 2 0 5
</pre></div></div>

<p>Logical arrays:
</p><div class="example">
<div class="group"><pre class="example-preformatted">&gt;&gt; a = [true, false, true]
a = 1 0 1
&gt;&gt; d(5) = true
d = 1 0 1 0 1
</pre></div></div>

<p>Character arrays:
</p><div class="example">
<div class="group"><pre class="example-preformatted">&gt;&gt; a = &quot;abc&quot;
a = abc
&gt;&gt; a(5) = &quot;d&quot;
a = abcd
&gt;&gt; double (a)
ans = 97 98 99 0 100
</pre></div></div>

<p>Cell arrays:
</p><div class="example">
<div class="group"><pre class="example-preformatted">&gt;&gt; e = {1, &quot;foo&quot;, [3, 4]};
&gt;&gt; e(5) = &quot;bar&quot;
e =
{
  [1,1] = 1
  [1,2] = foo
  [1,3] =

     3   4

  [1,4] = [](0x0)
  [1,5] = bar
}
</pre></div></div>

<p>Struct arrays:
</p><div class="example">
<div class="group"><pre class="example-preformatted">&gt;&gt; a = struct(&quot;foo&quot;,1,&quot;bar&quot;,2);
&gt;&gt; a(3) = struct(&quot;foo&quot;,3,&quot;bar&quot;,9)
a =

  1x3 struct array containing the fields:

    foo
    bar

&gt;&gt; a.foo
ans = 1
ans = [](0x0)
ans = 3
&gt;&gt; a.bar
ans = 2
ans = [](0x0)
ans = 9
</pre></div></div>

<p>Note that Octave currently is unable to concatenate arbitrary object types
into arrays.  Such behavior must be explicitly defined within the object class
or attempts at concatenation will result in an error.
See <a class="xref" href="Object-Oriented-Programming.html">Object Oriented Programming</a>
</p>
<p>Assigning an empty matrix &lsquo;<samp class="samp">[]</samp>&rsquo; works in most cases to allow you to
delete rows or columns of matrices and vectors.  See <a class="xref" href="Empty-Matrices.html">Empty Matrices</a>.
For example, given a 4 by 5 matrix <var class="var">A</var>, the assignment
</p>
<div class="example">
<pre class="example-preformatted">A (3, :) = []
</pre></div>

<p>deletes the third row of <var class="var">A</var>, and the assignment
</p>
<div class="example">
<pre class="example-preformatted">A (:, 1:2:5) = []
</pre></div>

<p>deletes the first, third, and fifth columns.
</p>
<p>Deleting part of an array object will necessarily resize the object.  When the
deletion allows for consistent size reduction across a dimension, e.g., one
element of a vector, or one row or column of a matrix, the size along that
dimension will be reduced while preserving dimensionality.  If, however,
dimensionality cannot be maintained, the object will be reshaped into a vector
following column-wise element ordering:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">&gt;&gt; a = [1, 2, 3, 4; 5, 6, 7, 8]
a =
   1   2   3   4
   5   6   7   8
&gt;&gt; a(:, 3) = []
a =
   1   2   4
   5   6   8
&gt;&gt; a(4) = []
a = 1 5 2 4 8
</pre></div></div>

<p>An assignment is an expression, so it has a value.  Thus, <code class="code">z = 1</code>
as an expression has the value 1.  One consequence of this is that you
can write multiple assignments together:
</p>
<div class="example">
<pre class="example-preformatted">x = y = z = 0
</pre></div>

<p>stores the value 0 in all three variables.  It does this because the
value of <code class="code">z = 0</code>, which is 0, is stored into <code class="code">y</code>, and then
the value of <code class="code">y = z = 0</code>, which is 0, is stored into <code class="code">x</code>.
</p>
<p>This is also true of assignments to lists of values, so the following is
a valid expression
</p>
<div class="example">
<pre class="example-preformatted">[a, b, c] = [u, s, v] = svd (a)
</pre></div>

<p>that is exactly equivalent to
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">[u, s, v] = svd (a)
a = u
b = s
c = v
</pre></div></div>

<p>In expressions like this, the number of values in each part of the
expression need not match.  For example, the expression
</p>
<div class="example">
<pre class="example-preformatted">[a, b] = [u, s, v] = svd (a)
</pre></div>

<p>is equivalent to
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">[u, s, v] = svd (a)
a = u
b = s
</pre></div></div>

<p>The number of values on the left side of the expression can, however,
not exceed the number of values on the right side.  For example, the
following will produce an error.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">[a, b, c, d] = [u, s, v] = svd (a);
-| error: element number 4 undefined in return list
</pre></div></div>

<p>The symbol <code class="code">~</code> may be used as a placeholder in the list of lvalues,
indicating that the corresponding return value should be ignored and not stored
anywhere:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">[~, s, v] = svd (a);
</pre></div></div>

<p>This is cleaner and more memory efficient than using a dummy variable.
The <code class="code">nargout</code> value for the right-hand side expression is not affected.
If the assignment is used as an expression, the return value is a
comma-separated list with the ignored values dropped.
</p>
<a class="index-entry-id" id="index-_002b_003d"></a>
<p>A very common programming pattern is to increment an existing variable
with a given value, like this
</p>
<div class="example">
<pre class="example-preformatted">a = a + 2;
</pre></div>

<p>This can be written in a clearer and more condensed form using the
<code class="code">+=</code> operator
</p>
<div class="example">
<pre class="example-preformatted">a += 2;
</pre></div>

<a class="index-entry-id" id="index-_002d_003d"></a>
<a class="index-entry-id" id="index-_002a_003d"></a>
<a class="index-entry-id" id="index-_002f_003d"></a>
<p>Similar operators also exist for subtraction (<code class="code">-=</code>),
multiplication (<code class="code">*=</code>), and division (<code class="code">/=</code>).  An expression
of the form
</p>
<div class="example">
<pre class="example-preformatted"><var class="var">expr1</var> <var class="var">op</var>= <var class="var">expr2</var>
</pre></div>

<p>is evaluated as
</p>
<div class="example">
<pre class="example-preformatted"><var class="var">expr1</var> = (<var class="var">expr1</var>) <var class="var">op</var> (<var class="var">expr2</var>)
</pre></div>

<p>where <var class="var">op</var> can be either <code class="code">+</code>, <code class="code">-</code>, <code class="code">*</code>, or <code class="code">/</code>,
as long as <var class="var">expr2</var> is a simple expression with no side effects.  If
<var class="var">expr2</var> also contains an assignment operator, then this expression
is evaluated as
</p>
<div class="example">
<div class="group"><pre class="example-preformatted"><var class="var">temp</var> = <var class="var">expr2</var>
<var class="var">expr1</var> = (<var class="var">expr1</var>) <var class="var">op</var> <var class="var">temp</var>
</pre></div></div>

<p>where <var class="var">temp</var> is a placeholder temporary value storing the computed
result of evaluating <var class="var">expr2</var>.  So, the expression
</p>
<div class="example">
<pre class="example-preformatted">a *= b+1
</pre></div>

<p>is evaluated as
</p>
<div class="example">
<pre class="example-preformatted">a = a * (b+1)
</pre></div>

<p>and <em class="emph">not</em>
</p>
<div class="example">
<pre class="example-preformatted">a = a * b + 1
</pre></div>

<p>You can use an assignment anywhere an expression is called for.  For
example, it is valid to write <code class="code">x != (y = 1)</code> to set <code class="code">y</code> to 1
and then test whether <code class="code">x</code> equals 1.  But this style tends to make
programs hard to read.  Except in a one-shot program, you should rewrite
it to get rid of such nesting of assignments.  This is never very hard.
</p>
<a class="index-entry-id" id="index-increment-operator"></a>
<a class="index-entry-id" id="index-decrement-operator"></a>
<a class="index-entry-id" id="index-operators_002c-increment"></a>
<a class="index-entry-id" id="index-operators_002c-decrement"></a>

</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Increment-Ops.html">Increment Operators</a>, Previous: <a href="Boolean-Expressions.html">Boolean Expressions</a>, Up: <a href="Expressions.html">Expressions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
