<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Inheritance and Aggregation (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Inheritance and Aggregation (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Inheritance and Aggregation (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Object-Oriented-Programming.html" rel="up" title="Object Oriented Programming">
<link href="classdef-Classes.html" rel="next" title="classdef Classes">
<link href="Overloading-Objects.html" rel="prev" title="Overloading Objects">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="Inheritance-and-Aggregation">
<div class="nav-panel">
<p>
Next: <a href="classdef-Classes.html" accesskey="n" rel="next"><code class="code">classdef</code> Classes</a>, Previous: <a href="Overloading-Objects.html" accesskey="p" rel="prev">Overloading Objects</a>, Up: <a href="Object-Oriented-Programming.html" accesskey="u" rel="up">Object Oriented Programming</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Inheritance-and-Aggregation-1"><span>34.5 Inheritance and Aggregation<a class="copiable-link" href="#Inheritance-and-Aggregation-1"> &para;</a></span></h3>

<p>Using classes to build new classes is supported by Octave through the use of
both inheritance and aggregation.
</p>
<p>Class inheritance is provided by Octave using the <code class="code">class</code> function in the
class constructor.  As in the case of the polynomial class, the Octave
programmer will create a structure that contains the data fields required by
the class, and then call the <code class="code">class</code> function to indicate that an object
is to be created from the structure.  Creating a child of an existing object is
done by creating an object of the parent class and providing that object as the
third argument of the class function.
</p>
<p>This is most easily demonstrated by example.  Suppose the programmer needs a
FIR filter, i.e., a filter with a numerator polynomial but a denominator of 1.
In traditional Octave programming this would be performed as follows.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">&gt;&gt; x = [some data vector];
&gt;&gt; n = [some coefficient vector];
&gt;&gt; y = filter (n, 1, x);
</pre></div></div>

<p>The equivalent behavior can be implemented as a class <code class="code">@FIRfilter</code>.  The
constructor for this class is the file <samp class="file">FIRfilter.m</samp> in the class
directory <samp class="file">@FIRfilter</samp>.
</p>
<div class="example">
<pre class="verbatim">## -*- texinfo -*-
## @deftypefn  {} {} FIRfilter ()
## @deftypefnx {} {} FIRfilter (@var{p})
## Create a FIR filter with polynomial @var{p} as coefficient vector.
## @end deftypefn

function f = FIRfilter (p)

  if (nargin == 0)
    p = @polynomial ([1]);
  elseif (! isa (p, &quot;polynomial&quot;))
    error (&quot;@FIRfilter: P must be a polynomial object&quot;);
  endif

  f.polynomial = [];
  f = class (f, &quot;FIRfilter&quot;, p);

endfunction
</pre></div>

<p>As before, the leading comments provide documentation for the class
constructor.  This constructor is very similar to the polynomial class
constructor, except that a polynomial object is passed as the third argument to
the <code class="code">class</code> function, telling Octave that the <code class="code">FIRfilter</code> class will
be derived from the polynomial class.  The FIR filter class itself does not
have any data fields, but it must provide a struct to the <code class="code">class</code>
function.  Given that the <code class="code">@polynomial</code> constructor will add an element
named <var class="var">polynomial</var> to the object struct, the <code class="code">@FIRfilter</code> just
initializes a struct with a dummy field <var class="var">polynomial</var> which will later be
overwritten.
</p>
<p>Note that the sample code always provides for the case in which no arguments
are supplied.  This is important because Octave will call a constructor with
no arguments when loading objects from saved files in order to determine the
inheritance structure.
</p>
<p>A class may be a child of more than one class (see <a class="pxref" href="Built_002din-Data-Types.html#XREFclass">class</a>), and
inheritance may be nested.  There is no limitation to the number of parents or
the level of nesting other than memory or other physical issues.
</p>
<p>For the <code class="code">FIRfilter</code> class, more control about the object display is
desired.  Therefore, the <code class="code">display</code> method rather than the <code class="code">disp</code>
method is overloaded (see <a class="pxref" href="Class-Methods.html">Class Methods</a>).  A simple example might be
</p>
<div class="example">
<div class="group"><pre class="verbatim">function display (f)
  printf (&quot;%s.polynomial&quot;, inputname (1));
  disp (f.polynomial);
endfunction
</pre></div></div>

<p>Note that the <code class="code">FIRfilter</code>&rsquo;s display method relies on the <code class="code">disp</code>
method from the <code class="code">polynomial</code> class to actually display the filter
coefficients.  Furthermore, note that in the <code class="code">display</code> method it makes
sense to start the method with the line
<code class="code"><code class="code">printf (&quot;%s =&quot;, inputname (1))</code></code> to be consistent with the
rest of Octave which prints the variable name to be displayed followed by the
value.  In general it is not recommended to overload the <code class="code">display</code>
function.
</p>
<a class="anchor" id="XREFdisplay"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-display"><span class="category-def">: </span><span><strong class="def-name">display</strong> <code class="def-code-arguments">(<var class="var">obj</var>)</code><a class="copiable-link" href="#index-display"> &para;</a></span></dt>
<dd><p>Display the contents of the object <var class="var">obj</var> prepended by its name.
</p>
<p>The Octave interpreter calls the <code class="code">display</code> function whenever it needs
to present a class on-screen.  Typically, this would be a statement which
does not end in a semicolon to suppress output.  For example:
</p>
<div class="example">
<pre class="example-preformatted">myclass (...)
</pre></div>

<p>Or:
</p>
<div class="example">
<pre class="example-preformatted">myobj = myclass (...)
</pre></div>

<p>In general, user-defined classes should overload the <code class="code">disp</code> method to
avoid the default output:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">myobj = myclass (...)
  &rArr; myobj =

  &lt;class myclass&gt;
</pre></div></div>

<p>When overloading the <code class="code">display</code> method instead, one has to take care
of properly displaying the object&rsquo;s name.  This can be done by using the
<code class="code">inputname</code> function.
</p>

<p><strong class="strong">See also:</strong> <a class="ref" href="Terminal-Output.html#XREFdisp">disp</a>, <a class="ref" href="Built_002din-Data-Types.html#XREFclass">class</a>, <a class="ref" href="Defining-Indexing-And-Indexed-Assignment.html#XREFsubsref">subsref</a>, <a class="ref" href="Defining-Indexing-And-Indexed-Assignment.html#XREFsubsasgn">subsasgn</a>.
</p></dd></dl>


<p>Once a constructor and display method exist, it is possible to create an
instance of the class.  It is also possible to check the class type and examine
the underlying structure.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">octave:1&gt; f = FIRfilter (polynomial ([1 1 1]/3))
f.polynomial = 0.33333 + 0.33333 * X + 0.33333 * X ^ 2
octave:2&gt; class (f)
ans = FIRfilter
octave:3&gt; isa (f, &quot;FIRfilter&quot;)
ans =  1
octave:4&gt; isa (f, &quot;polynomial&quot;)
ans =  1
octave:5&gt; struct (f)
ans =

  scalar structure containing the fields:

polynomial = 0.33333 + 0.33333 * X + 0.33333 * X ^ 2
</pre></div></div>

<p>The only thing remaining to make this class usable is a method for processing
data.  But before that, it is usually desirable to also have a way of changing
the data stored in a class.  Since the fields in the underlying struct are
private by default, it is necessary to provide a mechanism to access the
fields.  The <code class="code">subsref</code> method may be used for both tasks.
</p>
<div class="example">
<pre class="verbatim">function r = subsref (f, x)

  switch (x.type)

    case &quot;()&quot;
      n = f.polynomial;
      r = filter (n.poly, 1, x.subs{1});

    case &quot;.&quot;
      fld = x.subs;
      if (! strcmp (fld, &quot;polynomial&quot;))
        error ('@FIRfilter/subsref: invalid property &quot;%s&quot;', fld);
      endif
      r = f.polynomial;

    otherwise
      error (&quot;@FIRfilter/subsref: invalid subscript type for FIR filter&quot;);

  endswitch

endfunction
</pre></div>

<p>The <code class="code">&quot;()&quot;</code> case allows us to filter data using the polynomial provided
to the constructor.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">octave:2&gt; f = FIRfilter (polynomial ([1 1 1]/3));
octave:3&gt; x = ones (5,1);
octave:4&gt; y = f(x)
y =

   0.33333
   0.66667
   1.00000
   1.00000
   1.00000
</pre></div></div>

<p>The <code class="code">&quot;.&quot;</code> case allows us to view the contents of the polynomial field.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">octave:1&gt; f = FIRfilter (polynomial ([1 1 1]/3));
octave:2&gt; f.polynomial
ans = 0.33333 + 0.33333 * X + 0.33333 * X ^ 2
</pre></div></div>

<p>In order to change the contents of the object a <code class="code">subsasgn</code> method is
needed.  For example, the following code makes the polynomial field publicly
writable
</p>
<div class="example">
<div class="group"><pre class="verbatim">function fout = subsasgn (f, index, val)

  switch (index.type)
    case &quot;.&quot;
      fld = index.subs;
      if (! strcmp (fld, &quot;polynomial&quot;))
        error ('@FIRfilter/subsasgn: invalid property &quot;%s&quot;', fld);
      endif
      fout = f;
      fout.polynomial = val;

    otherwise
      error (&quot;@FIRfilter/subsasgn: Invalid index type&quot;)
  endswitch

endfunction
</pre></div></div>

<p>so that
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">octave:1&gt; f = FIRfilter ();
octave:2&gt; f.polynomial = polynomial ([1 2 3])
f.polynomial = 1 + 2 * X + 3 * X ^ 2
</pre></div></div>

<p>Defining the FIRfilter<!-- /@w --> class as a child of the polynomial class implies
that a FIRfilter<!-- /@w --> object may be used any place that a polynomial object may
be used.  This is not a normal use of a filter.  It may be a more sensible
design approach to use aggregation rather than inheritance.  In this case, the
polynomial is simply a field in the class structure.  A class constructor for
the aggregation case might be
</p>
<div class="example">
<pre class="verbatim">## -*- texinfo -*-
## @deftypefn  {} {} FIRfilter ()
## @deftypefnx {} {} FIRfilter (@var{p})
## Create a FIR filter with polynomial @var{p} as coefficient vector.
## @end deftypefn

function f = FIRfilter (p)

  if (nargin == 0)
    f.polynomial = @polynomial ([1]);
  else
    if (! isa (p, &quot;polynomial&quot;))
      error (&quot;@FIRfilter: P must be a polynomial object&quot;);
    endif

    f.polynomial = p;
  endif

  f = class (f, &quot;FIRfilter&quot;);

endfunction
</pre></div>

<p>For this example only the constructor needs changing, and all other class
methods stay the same.
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="classdef-Classes.html"><code class="code">classdef</code> Classes</a>, Previous: <a href="Overloading-Objects.html">Overloading Objects</a>, Up: <a href="Object-Oriented-Programming.html">Object Oriented Programming</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
