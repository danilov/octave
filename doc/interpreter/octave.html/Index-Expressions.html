<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Index Expressions (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Index Expressions (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Index Expressions (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Expressions.html" rel="up" title="Expressions">
<link href="Calling-Functions.html" rel="next" title="Calling Functions">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
ul.mark-bullet {list-style-type: disc}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="section-level-extent" id="Index-Expressions">
<div class="nav-panel">
<p>
Next: <a href="Calling-Functions.html" accesskey="n" rel="next">Calling Functions</a>, Up: <a href="Expressions.html" accesskey="u" rel="up">Expressions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h3 class="section" id="Index-Expressions-1"><span>8.1 Index Expressions<a class="copiable-link" href="#Index-Expressions-1"> &para;</a></span></h3>

<a class="index-entry-id" id="index-_0028"></a>
<a class="index-entry-id" id="index-_0029"></a>
<a class="index-entry-id" id="index-_003a"></a>

<p>An <em class="dfn">index expression</em> allows you to reference or extract selected
elements of a vector, a matrix (2-D), or a higher-dimensional array.  Arrays
may be indexed in one of three ways:
<a class="ref" href="#XREFComponentIndexing">Component Indexing</a>, 
<a class="ref" href="#XREFLinearIndexing">Linear Indexing</a>, and
<a class="ref" href="#XREFLogicalIndexing">Logical Indexing</a>.
</p>
<a class="anchor" id="XREFComponentIndexing"></a><h4 class="subheading" id="Component-Indexing"><span>Component Indexing<a class="copiable-link" href="#Component-Indexing"> &para;</a></span></h4>
<a class="index-entry-id" id="index-Component-Indexing"></a>
<p>Component indices may be scalars, vectors, ranges, or the special operator
&lsquo;<samp class="samp">:</samp>&rsquo;, which selects entire rows, columns, or higher-dimensional slices.
</p>
<p>Component index expression consists of a set of parentheses enclosing <em class="math">M</em>
expressions separated by commas.  Each individual index value, or component,
is used for the respective dimension of the object that it is applied to.  In
other words, the first index component is used for the first dimension (rows)
of the object, the second index component is used for the second dimension
(columns) of the object, and so on.  The number of index components <em class="math">M</em>
defines the dimensionality of the index expression.  An index with two
components would be referred to as a 2-D index because it has two dimensions.
</p>
<p>In the simplest case, 1) all components are scalars, and 2) the dimensionality
of the index expression <em class="math">M</em> is equal to the dimensionality of the object
it is applied to.  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">A = reshape (1:8, 2, 2, 2)  # Create 3-D array
A =

ans(:,:,1) =

   1   3
   2   4

ans(:,:,2) =

   5   7
   6   8

A(2, 1, 2)   # second row, first column of second slice
             # in third dimension: ans = 6
</pre></div></div>

<p>The size of the returned object in a specific dimension is equal to the number
of elements in the corresponding component of the index expression.  When all
components are scalars, the result is a single output value.  However, if any
component is a vector or range then the returned values are the Cartesian
product of the indices in the respective dimensions.  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">A([1, 2], 1, 2) &equiv; [A(1,1,2); A(2,1,2)]
&rArr;
ans =
   5
   6
</pre></div></div>

<p>The total number of returned values is the product of the number of elements
returned for each index component.  In the example above, the total is 2*1*1 =
2 elements.
</p>
<p>Notice that the size of the returned object in a given dimension is equal to
the number of elements in the index expression for that dimension.  In the code
above, the first index component (<code class="code">[1, 2]</code>) was specified as a row vector,
but its shape is unimportant.  The important fact is that the component
specified two values, and hence the result must have a size of two in the first
dimension; and because the first dimension corresponds to rows, the overall
result is a column vector.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">A(1, [2, 1, 1], 1)    # result is a row vector: ans = [3, 1, 1]
A(ones (2, 2), 1, 1)  # result is a column vector: ans = [1; 1; 1; 1]
</pre></div></div>

<p>The first line demonstrates again that the size of the output in a given
dimension is equal to the number of elements in the respective indexing
component.  In this case, the output has three elements in the second dimension
(which corresponds to columns), so the result is a row vector.  The example
also shows how repeating entries in the index expression can be used to
replicate elements in the output.  The last example further proves that the
shape of the indexing component is irrelevant, it is only the number of
elements (2x2 = 4) which is important.
</p>
<p>The above rules apply whenever the dimensionality of the index expression
is greater than one (<em class="math">M &gt; 1</em>).  However, for one-dimensional index
expressions special rules apply and the shape of the output <strong class="strong">is</strong>
determined by the shape of the indexing component.  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">A([1, 2])  # result is a row vector: ans = [1, 2]
A([1; 2])  # result is a column vector: ans = [1; 2]
</pre></div></div>

<p>The shape rules for <var class="var">A</var>(<var class="var">P</var>) are:
</p>
<ul class="itemize mark-bullet">
<li>When at least one of <var class="var">A</var> or <var class="var">P</var> has two or more dimensions, then
<var class="var">A</var>(<var class="var">P</var>) takes the shape of <var class="var">P</var>.  This happens when at least one
of the variables is a 2-D matrix or an N-D array.

</li><li>When both <var class="var">A</var> and <var class="var">P</var> are 1-D vectors, then <var class="var">A</var>(<var class="var">P</var>) takes
the shape of <var class="var">A</var> itself.  In particular, when <var class="var">A</var> is a row vector, then
<var class="var">A</var>(<var class="var">P</var>) is also a row vector irrespective of <var class="var">P</var>&rsquo;s shape.  The
case when <var class="var">A</var> is a column vector is analogous.
</li></ul>

<a class="index-entry-id" id="index-_003a_002c-indexing-expressions"></a>

<p>A colon (&lsquo;<samp class="samp">:</samp>&rsquo;) may be used as an index component to select all of the
elements in a specified dimension.  Given the matrix,
</p>
<div class="example">
<pre class="example-preformatted">A = [1, 2; 3, 4]
</pre></div>

<p>all of the following expressions are equivalent and select the first row of the
matrix.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">A(1, [1, 2])  # row 1, columns 1 and 2
A(1, 1:2)     # row 1, columns in range 1-2
A(1, :)       # row 1, all columns
</pre></div></div>

<p>When a colon is used in the special case of 1-D indexing the result is always a
column vector.  Creating column vectors with a colon index is a very frequently
encountered code idiom and is faster and generally clearer than calling
<code class="code">reshape</code> for this case.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">A(:)    # result is column vector: ans = [1; 2; 3; 4]
A(:)'   # result is row vector: ans = [1, 2, 3, 4]
</pre></div></div>

<a class="index-entry-id" id="index-end_002c-indexing"></a>
<a class="index-entry-id" id="index-end_003a-and-_003aend"></a>

<p>In index expressions the keyword <code class="code">end</code> automatically refers to the last
entry for a particular dimension.  This magic index can also be used in ranges
and typically eliminates the needs to call <code class="code">size</code> or <code class="code">length</code> to
gather array bounds before indexing.  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">A(1:end/2)        # first half of A =&gt; [1, 2]
A(end + 1) = 5;   # append element
A(end) = [];      # delete element
A(1:2:end)        # odd elements of A =&gt; [1, 3]
A(2:2:end)        # even elements of A =&gt; [2, 4]
A(end:-1:1)       # reversal of A =&gt; [4, 3, 2, 1]
</pre></div></div>

<p>For more information see the <a class="ref" href="Keywords.html#XREFend"><code class="code">&quot;end&quot; keyword</code></a>.
</p>
<a class="anchor" id="XREFLinearIndexing"></a><h4 class="subheading" id="Linear-Indexing"><span>Linear Indexing<a class="copiable-link" href="#Linear-Indexing"> &para;</a></span></h4>
<a class="index-entry-id" id="index-Linear-Indexing"></a>
<p>It is permissible to use a 1-D index with a multi-dimensional object.  This is
also called <em class="dfn">linear indexing</em>.  In this case, the elements of the
multi-dimensional array are taken in column-first order like in Fortran.  That
is, the columns of the array are imagined to be stacked on top of each other
to form a column vector and then the single linear index is applied to this
vector.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">A = [1, 2, 3; 4, 5, 6; 7, 8, 9];

A(4)    # linear index of 4th element in 2-D array: ans = 2
A(3:5)  # result has shape of index component: ans = [7, 2, 5]
A([1, 2, 2, 1]) # result includes repeated elements: ans = [1, 4, 4, 1]
</pre></div></div>

<a class="anchor" id="XREFLogicalIndexing"></a><h4 class="subheading" id="Logical-Indexing"><span>Logical Indexing<a class="copiable-link" href="#Logical-Indexing"> &para;</a></span></h4>
<a class="index-entry-id" id="index-Logical-Indexing"></a>
<p>Logical values can also be used to index matrices and cell arrays.
When indexing with a logical array the result will be a vector containing
the values corresponding to <code class="code">true</code> parts of the logical array. The
following examples illustrates this.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">data = [ 1, 2; 3, 4 ];
idx = [true, false; false true];
data(idx)
     &rArr; ans = [ 1; 4 ]

idx = (data &lt;= 2);
data(idx)
     &rArr; ans = [ 1; 2 ]
</pre></div></div>

<p>Instead of creating the <code class="code">idx</code> array it is possible to replace
<code class="code">data(idx)</code> with <code class="code">data(&nbsp;data&nbsp;&lt;=&nbsp;2&nbsp;)</code><!-- /@w --> in the above code.
</p>
<p>While the size of the logical index expressions is usually the same as that
of the array being indexed, this is not a necessary condition.  If the logical
index is a different size than the array, then elements from the array are
matched against elements from the logical index based on their linear order,
just as with linear indexing.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">data = [ 1, 2, 3; 4, 5, 6 ];
idx = [ true, false, false, true ];
data(idx)
     &rArr; ans = [ 1 5 ]  # idx selected the 1st and 4th position elements
</pre></div></div>

<p>If the logical index is larger than then array an out of bounds error will
occur if any true values attempt to select a linear position larger than the
number of elements in the array.
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">idx = [ true, true, false; false, true, false; true; false; false ];
data(idx)
     &rArr; ans = [ 1; 2; 5; 3 ] # returns positions 1, 3, 4, 5 in a column

idx = [ true, true, false; false, true, false; true; false; true ];
data(idx)
     &rArr; error: a(9): out of bound 6 (dimensions are 2x3)
</pre></div></div>

<p>False elements of a logical index outside the array size are ignored, but when
the ninth element of the logical index is true it triggers an error as it tries
to select a nonexistent 9th element of the array.
</p>

<ul class="mini-toc">
<li><a href="Advanced-Indexing.html" accesskey="1">Advanced Indexing</a></li>
</ul>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Calling-Functions.html">Calling Functions</a>, Up: <a href="Expressions.html">Expressions</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
