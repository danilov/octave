<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Numeric Data Types (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Numeric Data Types (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Numeric Data Types (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="index.html" rel="up" title="Top">
<link href="Strings.html" rel="next" title="Strings">
<link href="Data-Types.html" rel="prev" title="Data Types">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="chapter-level-extent" id="Numeric-Data-Types">
<div class="nav-panel">
<p>
Next: <a href="Strings.html" accesskey="n" rel="next">Strings</a>, Previous: <a href="Data-Types.html" accesskey="p" rel="prev">Data Types</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h2 class="chapter" id="Numeric-Data-Types-1"><span>4 Numeric Data Types<a class="copiable-link" href="#Numeric-Data-Types-1"> &para;</a></span></h2>
<a class="index-entry-id" id="index-numeric-constant-1"></a>
<a class="index-entry-id" id="index-numeric-value-1"></a>

<p>A <em class="dfn">numeric constant</em> may be a scalar, a vector, or a matrix, and it may
contain complex values.
</p>
<p>The simplest form of a numeric constant, a scalar, is a single number.  Note
that by default numeric constants are represented within Octave by IEEE 754
double precision (binary64) floating-point format (complex constants are
stored as pairs of binary64 values).  It is, however, possible to represent
real integers as described in <a class="ref" href="Integer-Data-Types.html">Integer Data Types</a>.
</p>
<p>If the numeric constant is a real integer, it can be defined in decimal,
hexadecimal, or binary notation.  Hexadecimal notation starts with &lsquo;<samp class="samp">0x</samp>&rsquo; or
&lsquo;<samp class="samp">0X</samp>&rsquo;, binary notation starts with &lsquo;<samp class="samp">0b</samp>&rsquo; or &lsquo;<samp class="samp">0B</samp>&rsquo;, otherwise
decimal notation is assumed.  As a consequence, &lsquo;<samp class="samp">0b</samp>&rsquo; is not a hexadecimal
number, in fact, it is not a valid number at all.
</p>
<p>For better readability, digits may be partitioned by the underscore separator
&lsquo;<samp class="samp">_</samp>&rsquo;, which is ignored by the Octave interpreter.  Here are some examples
of real-valued integer constants, which all represent the same value and are
internally stored as binary64:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">42            # decimal notation
0x2A          # hexadecimal notation
0b101010      # binary notation
0b10_1010     # underscore notation
round (42.1)  # also binary64
</pre></div></div>

<p>In decimal notation, the numeric constant may be denoted as decimal fraction
or even in scientific (exponential) notation.  Note that this is not possible
for hexadecimal or binary notation.  Again, in the following example all
numeric constants represent the same value:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">.105
1.05e-1
.00105e+2
</pre></div></div>

<p>Unlike most programming languages, complex numeric constants are denoted as
the sum of real and imaginary parts.  The imaginary part is denoted by a
real-valued numeric constant followed immediately by a complex value indicator
(&lsquo;<samp class="samp">i</samp>&rsquo;, &lsquo;<samp class="samp">j</samp>&rsquo;, &lsquo;<samp class="samp">I</samp>&rsquo;, or &lsquo;<samp class="samp">J</samp>&rsquo; which represents
  <code class="code">sqrt (-1)</code>).
No spaces are allowed between the numeric constant and the complex value
indicator.  Some examples of complex numeric constants that all represent the
same value:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">3 + 42i
3 + 42j
3 + 42I
3 + 42J
3.0 + 42.0i
3.0 + 0x2Ai
3.0 + 0b10_1010i
0.3e1 + 420e-1i
</pre></div></div>

<a class="anchor" id="XREFdouble"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-double"><span class="category-def">: </span><span><code class="def-type"><var class="var">y</var> =</code> <strong class="def-name">double</strong> <code class="def-code-arguments">(<var class="var">x</var>)</code><a class="copiable-link" href="#index-double"> &para;</a></span></dt>
<dd><p>Convert <var class="var">x</var> to double precision type.
</p>
<p><strong class="strong">See also:</strong> <a class="ref" href="Single-Precision-Data-Types.html#XREFsingle">single</a>.
</p></dd></dl>


<a class="anchor" id="XREFcomplex"></a><span style="display:block; margin-top:-4.5ex;">&nbsp;</span>


<dl class="first-deftypefn">
<dt class="deftypefn" id="index-complex"><span class="category-def">: </span><span><code class="def-type"><var class="var">z</var> =</code> <strong class="def-name">complex</strong> <code class="def-code-arguments">(<var class="var">x</var>)</code><a class="copiable-link" href="#index-complex"> &para;</a></span></dt>
<dt class="deftypefnx def-cmd-deftypefn" id="index-complex-1"><span class="category-def">: </span><span><code class="def-type"><var class="var">z</var> =</code> <strong class="def-name">complex</strong> <code class="def-code-arguments">(<var class="var">re</var>, <var class="var">im</var>)</code><a class="copiable-link" href="#index-complex-1"> &para;</a></span></dt>
<dd><p>Return a complex value from real arguments.
</p>
<p>With 1 real argument <var class="var">x</var>, return the complex result
<code class="code"><var class="var">x</var>&nbsp;+&nbsp;0i</code><!-- /@w -->.
</p>
<p>With 2 real arguments, return the complex result
<code class="code"><var class="var">re</var>&nbsp;+&nbsp;<var class="var">im</var>i</code><!-- /@w -->.
<code class="code">complex</code> can often be more convenient than expressions such as
<code class="code">a&nbsp;+&nbsp;b*i</code><!-- /@w -->.
For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">complex ([1, 2], [3, 4])
  &rArr; [ 1 + 3i   2 + 4i ]
</pre></div></div>

<p><strong class="strong">See also:</strong> <a class="ref" href="Complex-Arithmetic.html#XREFreal">real</a>, <a class="ref" href="Complex-Arithmetic.html#XREFimag">imag</a>, <a class="ref" href="Predicates-for-Numeric-Objects.html#XREFiscomplex">iscomplex</a>, <a class="ref" href="Complex-Arithmetic.html#XREFabs">abs</a>, <a class="ref" href="Complex-Arithmetic.html#XREFarg">arg</a>.
</p></dd></dl>



<ul class="mini-toc">
<li><a href="Matrices.html" accesskey="1">Matrices</a></li>
<li><a href="Ranges.html" accesskey="2">Ranges</a></li>
<li><a href="Single-Precision-Data-Types.html" accesskey="3">Single Precision Data Types</a></li>
<li><a href="Integer-Data-Types.html" accesskey="4">Integer Data Types</a></li>
<li><a href="Bit-Manipulations.html" accesskey="5">Bit Manipulations</a></li>
<li><a href="Logical-Values.html" accesskey="6">Logical Values</a></li>
<li><a href="Automatic-Conversion-of-Data-Types.html" accesskey="7">Automatic Conversion of Data Types</a></li>
<li><a href="Predicates-for-Numeric-Objects.html" accesskey="8">Predicates for Numeric Objects</a></li>
</ul>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Strings.html">Strings</a>, Previous: <a href="Data-Types.html">Data Types</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
