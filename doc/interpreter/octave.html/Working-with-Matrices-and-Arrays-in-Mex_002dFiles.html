<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Working with Matrices and Arrays in Mex-Files (GNU Octave (version 9.2.0))</title>

<meta name="description" content="Working with Matrices and Arrays in Mex-Files (GNU Octave (version 9.2.0))">
<meta name="keywords" content="Working with Matrices and Arrays in Mex-Files (GNU Octave (version 9.2.0))">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Mex_002dFiles.html" rel="up" title="Mex-Files">
<link href="Character-Strings-in-Mex_002dFiles.html" rel="next" title="Character Strings in Mex-Files">
<link href="Getting-Started-with-Mex_002dFiles.html" rel="prev" title="Getting Started with Mex-Files">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
-->
</style>
<link rel="stylesheet" type="text/css" href="octave.css">


</head>

<body lang="en">
<div class="subsection-level-extent" id="Working-with-Matrices-and-Arrays-in-Mex_002dFiles">
<div class="nav-panel">
<p>
Next: <a href="Character-Strings-in-Mex_002dFiles.html" accesskey="n" rel="next">Character Strings in Mex-Files</a>, Previous: <a href="Getting-Started-with-Mex_002dFiles.html" accesskey="p" rel="prev">Getting Started with Mex-Files</a>, Up: <a href="Mex_002dFiles.html" accesskey="u" rel="up">Mex-Files</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<h4 class="subsection" id="Working-with-Matrices-and-Arrays-in-Mex_002dFiles-1"><span>A.2.2 Working with Matrices and Arrays in Mex-Files<a class="copiable-link" href="#Working-with-Matrices-and-Arrays-in-Mex_002dFiles-1"> &para;</a></span></h4>

<p>The basic mex type of all variables is <code class="code">mxArray</code>.  Any object, such as a
matrix, cell array, or structure, is stored in this basic type.  <code class="code">mxArray</code>
serves essentially the same purpose as the <code class="code">octave_value</code> class in
oct-files in that it acts as a container for all the more specialized types.
</p>
<p>The <code class="code">mxArray</code> structure contains at a minimum, the name of the variable it
represents, its dimensions, its type, and whether the variable is real or
complex.  It can also contain a number of additional fields depending on the
type of the <code class="code">mxArray</code>.  There are a number of functions to create
<code class="code">mxArray</code> structures, including <code class="code">mxCreateDoubleMatrix</code>,
<code class="code">mxCreateCellArray</code>, <code class="code">mxCreateSparse</code>, and the generic
<code class="code">mxCreateNumericArray</code>.
</p>
<p>The basic function to access the data in an array is <code class="code">mxGetPr</code>.  Because
the mex interface assumes that real and imaginary parts of a complex array are
stored separately, there is an equivalent function <code class="code">mxGetPi</code> that gets the
imaginary part.  Both of these functions are only for use with double precision
matrices.  The generic functions <code class="code">mxGetData</code> and <code class="code">mxGetImagData</code>
perform the same operation for all matrix types.  For example:
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">mxArray *m;
mwSize *dims;
UINT32_T *pr;

dims = (mwSize *) mxMalloc (2 * sizeof (mwSize));
dims[0] = 2; dims[1] = 2;
m = mxCreateNumericArray (2, dims, mxUINT32_CLASS, mxREAL);
pr = (UINT32_T *) mxGetData (m);
</pre></div></div>

<p>There are also the functions <code class="code">mxSetPr</code>, etc., that perform the inverse,
and set the data of an array to use the block of memory pointed to by the
argument of <code class="code">mxSetPr</code>.
</p>
<p>Note the type <code class="code">mwSize</code> used above, and also <code class="code">mwIndex</code>, are defined as
the native precision of the indexing in Octave on the platform on which the
mex-file is built.  This allows both 32- and 64-bit platforms to support
mex-files.  <code class="code">mwSize</code> is used to define array dimensions and the maximum
number or elements, while <code class="code">mwIndex</code> is used to define indexing into
arrays.
</p>
<p>An example that demonstrates how to work with arbitrary real or complex double
precision arrays is given by the file <samp class="file">mypow2.c</samp> shown below.
</p>
<div class="example">
<pre class="verbatim">#include &quot;mex.h&quot;

void
mexFunction (int nlhs, mxArray *plhs[],
             int nrhs, const mxArray *prhs[])
{
  mwSize n;
  mwIndex i;
  double *vri, *vro;

  if (nrhs != 1 || ! mxIsDouble (prhs[0]))
    mexErrMsgTxt (&quot;ARG1 must be a double matrix&quot;);

  n = mxGetNumberOfElements (prhs[0]);
  plhs[0] = mxCreateNumericArray (mxGetNumberOfDimensions (prhs[0]),
                                  mxGetDimensions (prhs[0]),
                                  mxGetClassID (prhs[0]),
                                  mxIsComplex (prhs[0]));
  vri = mxGetPr (prhs[0]);
  vro = mxGetPr (plhs[0]);

  if (mxIsComplex (prhs[0]))
    {
      double *vii, *vio;
      vii = mxGetPi (prhs[0]);
      vio = mxGetPi (plhs[0]);

      for (i = 0; i &lt; n; i++)
        {
          vro[i] = vri[i] * vri[i] - vii[i] * vii[i];
          vio[i] = 2 * vri[i] * vii[i];
        }
    }
  else
    {
      for (i = 0; i &lt; n; i++)
        vro[i] = vri[i] * vri[i];
    }
}
</pre></div>

<p>An example of its use is
</p>
<div class="example">
<div class="group"><pre class="example-preformatted">b = randn (4,1) + 1i * randn (4,1);
all (b.^2 == mypow2 (b))
&rArr; 1
</pre></div></div>

<p>The example above uses the functions <code class="code">mxGetDimensions</code>,
<code class="code">mxGetNumberOfElements</code>, and <code class="code">mxGetNumberOfDimensions</code> to work with
the dimensions of multi-dimensional arrays.  The functions <code class="code">mxGetM</code>, and
<code class="code">mxGetN</code> are also available to find the number of rows and columns in a
2-D matrix (MxN matrix).
</p>
</div>
<hr>
<div class="nav-panel">
<p>
Next: <a href="Character-Strings-in-Mex_002dFiles.html">Character Strings in Mex-Files</a>, Previous: <a href="Getting-Started-with-Mex_002dFiles.html">Getting Started with Mex-Files</a>, Up: <a href="Mex_002dFiles.html">Mex-Files</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
